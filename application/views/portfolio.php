        <?php include 'layouts/header.php'; ?>

        <section class="home-section home-full-height home-section_inner" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(<?php echo base_url()?>assets/images/portfolio-bg.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content_inner">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">Turning ideas into <br>Reality</div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <!--<a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="section_padd" id="portfolio-container">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>OUR PORTFOLIO</h2>
                    </div>
                </div>
            </div>
             <div class="container">
        <div class="row">
        <div align="center">
            <button class="btn btn-default filter-button" data-filter="all">All</button>
            <button class="btn btn-default filter-button" data-filter="govt">Government</button>
            <button class="btn btn-default filter-button" data-filter="software">Software</button>
            <button class="btn btn-default filter-button" data-filter="entertainment">Entertainment</button>
            <button class="btn btn-default filter-button" data-filter="industrial">Industrial</button>
            <button class="btn btn-default filter-button" data-filter="auto">Auto Motives</button>
            <button class="btn btn-default filter-button" data-filter="ecomm">E-Commerce</button>
            <button class="btn btn-default filter-button" data-filter="medical">Medical</button>
        </div>
        <br/>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter govt">
               <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/apdascac" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/apdascac.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/apdascac" target="_blank"><h4>A.P.D.A.S.C.A.C</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/apdascac" target="_blank"><p><i class="fa fa-tag"></i>Government</p></a>
                </div>
            </div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter software">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/accusaga" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/accusaga1.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/accusaga" target="_blank"><h4>Accusaga</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/accusaga" target="_blank"><p><i class="fa fa-tag"></i>Software</p></a>
                </div>
            </div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter auto">
               <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/budgetcars" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/budget.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/budgetcars" target="_blank"><h4>Budget Cars</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/budgetcars" target="_blank"><p><i class="fa fa-tag"></i>Car Traders</p></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter entertainment">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/conduitmuzic" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/cm.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/conduitmuzic" target="_blank"><h4>Conduit Muzic</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/conduitmuzic" target="_blank"><p><i class="fa fa-tag"></i>Music</p></a>
                </div>
            </div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter software">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/coresofttech" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/core.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/coresofttech" target="_blank"><h4>Core Soft Technologies</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/coresofttech" target="_blank"><p><i class="fa fa-tag"></i>Software</p></a>
                </div>
            </div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter software">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/sumascorp" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/sumaa.png" class="img-responsive animate"></a>
                    </div>
                        <a href="<?php echo base_url();?>Portfolio/sumascorp" target="_blank"><h4>sumascorp</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/sumascorp" target="_blank"><p><i class="fa fa-tag"></i>Software</p></a>
                </div>
            </div>
<div class="clearfix"></div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter software">
                <div class="port_cont">
                    <div class="port_img">
                        <img src="<?php echo base_url();?>assets/images/portfolio/h3s.png" class="img-responsive animate"></a>
                    </div>
                        <a href="<?php echo base_url();?>Portfolio/h3sglobal" target="_blank"><h4>H3SGlobal.US</h4></a>
                        <a href="<?php echo base_url();?>Portfolio/h3sglobal" target="_blank"><p><i class="fa fa-tag"></i>Software</p></a>
                </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter ecomm">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/sarithareddy" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/saritha.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/sarithareddy" target="_blank"><h4>Sarithareddy Studios</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/sarithareddy" target="_blank"><p><i class="fa fa-tag"></i>E-Commerce</p></a>
                </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter entertainment">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/somaraspa" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/somara1.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/somaraspa" target="_blank"><h4>Somara Spa</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/somaraspa" target="_blank"><p><i class="fa fa-tag"></i>Spa</p></a>
                </div>
            </div>

<div class="clearfix"></div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter software">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/amerident" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/amerident1.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/amerident" target="_blank"><h4>Amerident</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/amerident" target="_blank"><p><i class="fa fa-tag"></i>Dental Software</p></a>
                </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter industrail">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/sresta" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/srestaa1.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/sresta" target="_blank"><h4>Sresta</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/sresta" target="_blank"><p><i class="fa fa-tag"></i>Electrical</p></a>
                </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter industrial">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/jbpackers" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/jbpackers.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/jbpackers" target="_blank"><h4>JB Reliable Packers</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/jbpackers" target="_blank"><p><i class="fa fa-tag"></i>Packers & Movers</p></a>
                </div>
            </div>
        </div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter software">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/echelon" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/etch.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/echelon" target="_blank"><h4>Echelon</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/echelon" target="_blank"><p><i class="fa fa-tag"></i>Software</p></a>
                </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter software">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/sahakar" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/sahakar.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/sahakar" target="_blank"><h4>Sahakar Solutions</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/sahakar" target="_blank"><p><i class="fa fa-tag"></i>Software</p></a>
                </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter industrial">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/fluidpower" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/fluid.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/fluidpower" target="_blank"><h4>Fluid Power Systems</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/fluidpower" target="_blank"><p><i class="fa fa-tag"></i>Industrial</p></a>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter industrial">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/kakatiya" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/kakatiya.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/kakatiya" target="_blank"><h4>Kakatiya Overseas</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/kakatiya" target="_blank"><p><i class="fa fa-tag"></i>Industrial</p></a>
                </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter medical">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/tulasi" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/tulasi.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/tulasi" target="_blank"><h4>Tulasi Hospitals</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/tulasi" target="_blank"><p><i class="fa fa-tag"></i>Medical</p></a>
                </div>
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter software">
                <div class="port_cont">
                    <div class="port_img">
                        <a href="<?php echo base_url();?>Portfolio/h3sglobal1" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/h3s1.png" class="img-responsive animate"></a>
                    </div>
                    <a href="<?php echo base_url();?>Portfolio/h3sglobal1" target="_blank"><h4>H3SGlobal.com</h4></a>
                    <a href="<?php echo base_url();?>Portfolio/h3sglobal1" target="_blank"><p><i class="fa fa-tag"></i>Software</p></a>
                </div>
            </div>
        </div>
    
            
        </section>
               <?php include 'layouts/footer.php'; ?>

        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a>
        </div>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
    <!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script>
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        //### Variables
        var player;
        var playerModal = $('#playerModal');

        //### Youtube API
        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                height: '390',
                width: '640',
                videoId: 'L2Ew6JzfZC8'
            });
        }

        //### Modal Controls (http://getbootstrap.com/javascript/#modals)
        // Modal when show, begin to play video
        playerModal.on('show.bs.modal', function(e) {
            player.playVideo();
        });

        // Modal when hidden, pause or stop playing video
        playerModal.on('hidden.bs.modal', function(e) {
            player.pauseVideo();
            //player.stopVideo();
        });
    </script>
    <script>
        //<!--    portfolio  JS  =================    -->

        $(function() {
            var selectedClass = "";
            $(".fil-cat").click(function() {
                selectedClass = $(this).attr("data-rel");
                $("#portfolio").fadeTo(100, 0.1);
                $("#portfolio div").not("." + selectedClass).fadeOut().removeClass('scale-anm');
                setTimeout(function() {
                    $("." + selectedClass).fadeIn().addClass('scale-anm');
                    $("#portfolio").fadeTo(300, 1);
                }, 300);

            });
        });
    </script>
<script>
    $(document).ready(function(){

    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        
        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        }
        else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
            
        }
    });
    
    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");

});
</script>