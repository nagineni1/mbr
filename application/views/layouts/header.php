<!DOCTYPE html>
<html lang="en-US" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MBR Informatics | Home </title>
  
    <link rel="manifest" href="http://markup.themewagon.com/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link href="<?php echo base_url();?>assets/lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/lib/animate.css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/lib/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/lib/et-line-font/et-line-font.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/lib/flexslider/flexslider.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link id="color-scheme" href="<?php echo base_url();?>assets/css/colors/default.css" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110113145-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-110113145-1');
</script>

</head>

<body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
    <main>
        <div class="page-loader">
            <div class="loader">Loading...</div>
        </div>
        <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle toggle1" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button><a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/images/logo.png" class="img-responsive"></a>
                </div>
                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li><a href="<?php echo base_url();?>About">About</a></li>
                        <li class="dropdown"><a class="dropdown-toggle" href="<?php echo base_url();?>Services" data-toggle="dropdown">Services</a>
                            <ul class="dropdown-menu submenu1">
                                <li class="dropdown"><a class="dropdown-toggle" href="<?php echo base_url();?>Services/webdesigning" data-toggle="dropdown">Web Designing</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url();?>Services/responsivedesign">Responsive Design</a></li>
                                        <li><a href="<?php echo base_url();?>Services/logodesign">Logo Design</a></li>
                                        <li><a href="<?php echo base_url();?>Services/broucherdesign">Broucher Design</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url();?>Services/webdevelopment">Web Development</a></li>
                                <li><a href="<?php echo base_url();?>Services/ecommerce">E-Commerce Development</a></li>
                                <li class="dropdown"><a class="dropdown-toggle" href="<?php echo base_url();?>Services/digitalmarketing" data-toggle="dropdown">Digital Marketing</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url();?>Services/seo">SEO</a>
                                        </li>
                                        <li><a href="<?php echo base_url();?>Services/sem">SEM</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a class="dropdown-toggle" href="<?php echo base_url();?>Services/consultingservices" data-toggle="dropdown">Consulting Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url();?>Services/strategy">Strategy & Architecture</a></li>
                                        <li><a href="<?php echo base_url();?>Services/business">Business Transformation</a></li>
                                        <li><a href="<?php echo base_url();?>Services/digital">Digital Transformation</a></li>
                                        <li><a href="<?php echo base_url();?>Services/insights">Insights</a></li>
                                        <li><a href="<?php echo base_url();?>Services/learning">Change & Learning</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a class="dropdown-toggle" href="<?php echo base_url();?>Services/businessservices" data-toggle="dropdown">Business Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url();?>Services/dataanalytics">Data Analytics</a></li>
                                        <li><a href="<?php echo base_url();?>Services/salesforce">Salesforce</a></li>
                                        <li><a href="<?php echo base_url();?>Services/businessapplications">Business Applications</a></li>
                                        <li><a href="<?php echo base_url();?>Services/sap">SAP</a></li>
                                        <li><a href="<?php echo base_url();?>Services/oracle">Oracle</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a class="dropdown-toggle" href="<?php echo base_url();?>Services/technologyservices" data-toggle="dropdown">Technology Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url();?>Services/applicationdevelopment">Application Development & Maintenance</a></li>
                                        <li><a href="<?php echo base_url();?>Services/iot">Internet Of Things</a></li>
                                        <li><a href="<?php echo base_url();?>Services/digitalprocessautomation">Digital Process Automation</a></li>
                                        <li><a href="<?php echo base_url();?>Services/testing">Testing</a></li>
                                        <li><a href="<?php echo base_url();?>Services/enterprisemobility">Enterprise Mobility</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a class="dropdown-toggle" href="<?php echo base_url();?>Services/outsourcing" data-toggle="dropdown">Outsourcing Services</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url();?>Services/applicationoutsourcing">Application Outsourcing</a></li>
                                        <li><a href="<?php echo base_url();?>Services/businessprocessoutsourcing">Business Process Outsourcing</a></li>
                                        <li><a href="<?php echo base_url();?>Services/finance">Finance & Accounting</a></li>
                                        <li><a href="<?php echo base_url();?>Services/humanresource">Human Resources</a></li>
                                        <li><a href="<?php echo base_url();?>Services/procurement">Sourcing & Procurement</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url();?>Portfolio">Portfolio</a></li>
                        <li><a href="<?php echo base_url();?>Careers">Careers</a></li>
                        <li><a href="<?php echo base_url();?>Contact">Contact Us</a></li>
                    </ul>
                </div>
            </div>
        </nav>