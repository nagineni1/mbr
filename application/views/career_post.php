        <?php include 'layouts/header.php'; ?>

        <section class="home-section home-full-height home-section_inner" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(<?php echo base_url()?>assets/images/careers-bg.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content_inner">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">We Build Awesome
                                    <br>Websites</div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="section_padd" id="portfolio-container">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>Post Your Requirement</h2>
                    </div>
                </div>
            </div>
            <div class="container">
              <div class="row">
                <div class="col col-md-12">
                     
            <?php if(isset($success)) echo '<div class="alert alert-success">'.$success.'</div>'; ?>
        
                  <form action="" method="post" enctype="multipart/form-data">
                      <div class="form-group required">
                        <label for="jobtitle" class='control-label'>Job Title</label>
                        <input type="text" class="form-control" id="jobtitle"  name="jobtitle" aria-describedby="emailHelp" placeholder="Ex: Senior Mobile Developer" required>
                      </div>
                      <div class="form-group required">
                        <label for="location" class='control-label'>Location</label>
                        <input type="password" class="form-control" name="location" id="location" placeholder="Ex:Hyderabad" required>
                      </div>
                      <div class="form-group required">
                        <label for="jobtype" class='control-label'>Job Type</label>
                        <select class="form-control" name="jobtype" id="jobtype" required>
                          <option>Full Time</option>
                          <option>Part Time</option>
                          <option>Contract</option>
                        </select>
                      </div>

                      <div class="form-group required">
                        <label for="jobdescription" class='control-label'>Job Description</label>
                        <textarea class="form-control" name="jobdescription" id="jobdescription" rows="7" required></textarea>
                      </div>
                      <div class="form-group required">
                        <label for="companyname" class='control-label'>Company Name</label>
                        <input type="text" class="form-control" name="companyname" id="companyname" placeholder="Ex:Infosys" required>
                      </div>
                      <div class="form-group">
                        <label for="companylogo">Company Logo</label>
                        <input type="file" class="form-control-file" name="companylogo" id="companylogo" aria-describedby="fileHelp" required>
                      </div>
                      <div class="form-group required">
                        <label for="howtoapply" class='control-label'>How to Apply</label>
                        <textarea class="form-control" name="howtoapply" id="howtoapply" rows="3" placeholder="For Example: Email your resume to jobs@xyz.com" required></textarea>
                      </div>
                        <ul class="media-date text-uppercase reviews list-inline">
                        <li class="aaaa"><button type="submit" class="btn btn-success">Publish</button></li>
                      </ul>
                </form>
            </div>
            </div>
            </div>
        </section>
        <?php include 'layouts/footer.php'; ?>

        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a>
        </div>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/<?php echo base_url()?>bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
    <!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script>
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        //### Variables
        var player;
        var playerModal = $('#playerModal');

        //### Youtube API
        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                height: '390',
                width: '640',
                videoId: 'L2Ew6JzfZC8'
            });
        }

        //### Modal Controls (http://getbootstrap.com/javascript/#modals)
        // Modal when show, begin to play video
        playerModal.on('show.bs.modal', function(e) {
            player.playVideo();
        });

        // Modal when hidden, pause or stop playing video
        playerModal.on('hidden.bs.modal', function(e) {
            player.pauseVideo();
            //player.stopVideo();
        });
    </script>
    <script>
        //<!--    portfolio  JS  =================    -->

        $(function() {
            var selectedClass = "";
            $(".fil-cat").click(function() {
                selectedClass = $(this).attr("data-rel");
                $("#portfolio").fadeTo(100, 0.1);
                $("#portfolio div").not("." + selectedClass).fadeOut().removeClass('scale-anm');
                setTimeout(function() {
                    $("." + selectedClass).fadeIn().addClass('scale-anm');
                    $("#portfolio").fadeTo(300, 1);
                }, 300);

            });
        });
    </script>
</body>

</html>