 <?php include 'layouts/header.php'; ?>
        <section class="home-section home-full-height home-section_inner" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(<?php echo base_url()?>assets/images/about-bg.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content_inner">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">Know more about us</div>
                                <div class="container">
                                    <div  class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <!--<a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>About Us</h2>
                        <p>Our experienced web architects, engineers and SEO specialists strive to provide creative Websites, easy to use CMS, comprehensive E-Commerce, flexible apps, professional Content and result-driven Digital Marketing solutions to turn your web entrepreneurial aspirations into a reality.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="hea_12">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="cont_1">
                                    <div class="gradient grad_trans grad_trans_inner abt_icons">
                                        <i class="fa fa-laptop" aria-hidden="true"></i>
                                    </div>
                                    <h3>Web Designing</h3>
                                    <p>Taking a deeper dive into your requirements, we aim to help taking your business to the next level.</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="cont_1">
                                    <div class="gradient grad_trans grad_trans_inner abt_icons">
                                        <i class="fa fa-cogs" aria-hidden="true"></i>
                                    </div>
                                    <h3>Web Development</h3>
                                    <p>Having worked with clients,the expertise we gained is passed on automatically to every new client.</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="cont_1">
                                    <div class="gradient grad_trans grad_trans_inner abt_icons">
                                       <i class="fa fa-mobile" aria-hidden="true"></i>
                                    </div>
                                    <h3>Mobile Applications</h3>
                                    <p>We provide high end mobile app services to achieve the best quality to boost business through your mobile app</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="video">
            <div class="video_section">
                <div class="container-fluid">
                    <div class="row">
                        <h4>Discover our Best Features</h4>
                        <h2>INTRODUCING MBR INFORMATICS</h2>
                        <div class="jumbotron text-center container">
                            <div class="col-md-8 col-md-offset-4">
                                <a href='#myModal' class='transition colelem btn_sub sub_btn' data-toggle='modal' data-target='#playerModal'>Video About MBR</a>
                            </div>
                        </div>
                        <div class="modal fade" id='playerModal' role='dialog' tabindex='-1'>
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button class="close" aria-label='Close' data-dismiss='modal', type='button'>
                                  <span aria-hidden='true'>×</span></button>
                                <h4 class="modal-title">Video title</h4>
                              </div>
                              <div class="modal-body">
                                <div class="embed-responsive embed-responsive-16by9">
                                  <div id="player" class="embed-responsive-item"></div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss='modal' type='button'>
                                    Close
                                </button>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>Our Featured Services</h2>
                    </div>
                </div>
                <div class="screen1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>Digital Marketing</h2>
                                    <p>Developing a long-term and reliable marketing strategy that will flourish and live a longer life, while ensuring your customers stay with you and tell the world the tales of your business’s awesomeness transforming our custom-made business strategies into real time success.</p>
                                    <h5>Our Digital Marketing Services</h5>
                                    <ul class="fa_screen1 pm0">
                                        <li><i class="fa fa-check"></i>Inbound Marketing</li>
                                        <li><i class="fa fa-check"></i>SEO , SEM , SMM</li>
                                        <li><i class="fa fa-check"></i>Analytics Consultation</li>
                                    </ul>
                                    <div class="transition colelem btn_sub">
                                       <a class="btn_sub1" href="<?php echo base_url();?>Services/digitalmarketing"><span>Read more</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pm0">
                              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                  <div class="item sds active">
                                    <img src="<?php echo base_url();?>assets/images/af/amerident.png" alt="Los Angeles">
                                  </div>
                                  <div class="item sds">
                                    <img src="<?php echo base_url();?>assets/images/af/apdascac.png" alt="Chicago">
                                  </div>
                                    <div class="item sds">
                                    <img src="<?php echo base_url();?>assets/images/af/cars.png" alt="Chicago">
                                  </div>
                                  <div class="item sds">
                                    <img src="<?php echo base_url();?>assets/images/af/conduit.png" alt="New York">
                                  </div>
                                    <div class="item sds">
                                    <img src="<?php echo base_url();?>assets/images/af/h3s.png" alt="New York">
                                  </div>
                                    <div class="item sds">
                                    <img src="<?php echo base_url();?>assets/images/af/jb.png" alt="New York">
                                  </div>
                                    <div class="item sds">
                                    <img src="<?php echo base_url();?>assets/images/af/saritha.png" alt="New York">
                                  </div>
                                    <div class="item sds">
                                    <img src="<?php echo base_url();?>assets/images/af/somara.png" alt="New York">
                                  </div>
                                    <div class="item sds">
                                    <img src="<?php echo base_url();?>assets/images/af/sresta.png" alt="New York">
                                  </div>
                                    <div class="item sds">
                                    <img src="<?php echo base_url();?>assets/images/af/sumas.png" alt="New York">
                                  </div>
                                </div>
                              </div>
                        </div>
                            <h2 class="view1">Quick View</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'layouts/footer.php'; ?>
        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a>
        </div>
    </main>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
<!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script>
        
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//### Variables
var player;
var playerModal = $('#playerModal');

//### Youtube API
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: 'L2Ew6JzfZC8'
    });
}

//### Modal Controls (http://getbootstrap.com/javascript/#modals)
// Modal when show, begin to play video
playerModal.on('show.bs.modal', function (e) {
    player.playVideo();
});

// Modal when hidden, pause or stop playing video
playerModal.on('hidden.bs.modal', function (e) {
    player.pauseVideo();
    //player.stopVideo();
});
    </script>
</body>

</html>
