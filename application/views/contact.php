        <?php include 'layouts/header.php'; ?>

        <section class="home-section home-full-height home-section_inner" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(<?php echo base_url()?>assets/images/contact-bg.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content_inner">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">We are here <br>for you</div>
                                <div class="container">
                                    <div  class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <!--<a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h5>WHAT WE CAN DO FOR YOU</h5>
                        <h2>GET IN TOUCH WITH US</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="hea_12">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="cont_1">
                                    <div class="gradient grad_trans grad_trans_inner abt_icons">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                    </div>
                                    <h3>E-mail Address :</h3>
                                    <p>contact@mbrinformatics.com</p>
                                    <p>info@mbrinformatics.com</p>
                                    <p>harshavardhan@mbrinformatics.com</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="cont_1">
                                    <div class="gradient grad_trans grad_trans_inner abt_icons">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                    </div>
                                    <h3>Phone Number :</h3>
                                    <p>040 6456 2322</p>
                                    <p>+91 8019255777</p>
                                    <p>+91 8686768612</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="cont_1">
                                    <div class="gradient grad_trans grad_trans_inner abt_icons">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    </div>
                                    <h3>Office Address :</h3>
                                    <p>Plot No. 658, 3rd Floor, Ayyappa Socitey, Madhapur, Hyderabad, <br>Telangana, 500081</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="video">
            <div class="video_section">
                <div class="container-fluid">
                    <div class="row">
                        <h4>Discover our Best Features</h4>
                        <h2>VIDEO ABOUT MBR</h2>
                        <div class="jumbotron text-center container">
                            <div class="col-md-8 col-md-offset-4">
                                <a href='#myModal' class='transition colelem btn_sub sub_btn' data-toggle='modal' data-target='#playerModal'>Video About MBR</a>
                            </div>
                        </div>
                        <div class="modal fade" id='playerModal' role='dialog' tabindex='-1'>
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button class="close" aria-label='Close' data-dismiss='modal', type='button'>
                                  <span aria-hidden='true'>×</span></button>
                                <h4 class="modal-title">Video title</h4>
                              </div>
                              <div class="modal-body">
                                <div class="embed-responsive embed-responsive-16by9">
                                  <div id="player" class="embed-responsive-item"></div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss='modal' type='button'>
                                    Close
                                </button>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>CONTACT FORM</h2>
                    </div>
                </div>
                    <div class="contact_form">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-8 col-md-offset-2">
                        <div class="well text-center">
                            <form action="<?php echo base_url()?>Contact/sendmail" method="post">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input class="form-control" id="id_fname" maxlength="100" name="f_name" placeholder="First name *" type="text" required>
                                    </div>
                                </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                        <input class="form-control" id="id_lname" maxlength="100" name="l_name" placeholder="Last name *" type="text" required>
                                    </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                     <input class="form-control" id="id_email" name="email" placeholder="E-mail *" type="email" required>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                     <input class="form-control" id="id_assunto" maxlength="100" name="Phone" placeholder="Phone Number *" type="text" required>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                     <input class="form-control" id="id_assunto" maxlength="100" name="companyname" placeholder="Company Name" type="text" / required>
                                  </div>
                              </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <select class="form-control" id="Service" required>
                                      <option>Select Service</option>
                                      <option>Web Designing</option>
                                      <option>Web Development</option>
                                      <option>E-Commerce Development</option>
                                      <option>Digital Marketing</option>
                                      <option>Consulting Services</option>
                                      <option>Business Services</option>
                                      <option>Technology Services</option>
                                      <option>Outsourcing Services</option>
                                    </select>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="form-group">
                                     <textarea class="form-control" cols="40" id="id_mensagem" name="message" placeholder="Message *" rows="7" data-gramm="true" data-txt_gramm_id="ada73dea-e8d6-f606-09ad-cd5c85e12eb9" data-gramm_id="ada73dea-e8d6-f606-09ad-cd5c85e12eb9" spellcheck="false" data-gramm_editor="true" style="z-index: auto; position: relative; line-height: 22.8571px; font-size: 16px; transition: none; " required></textarea><grammarly-btn><div class="_1BN1N Kzi1t MoE_1 _2DJZN" style="z-index: 2; transform: translate(664px, 145px);"><div class="_1HjH7"><div title="Protected by Grammarly" class="_3qe6h">&nbsp;</div></div></div></grammarly-btn>
                                  </div>
                              </div>
                             <div class="col-md-12">
                                <div class="form-group">
                                    <!-- Replace data-sitekey with your own one, generated at https://www.google.com/recaptcha/admin -->
                                    <div class="g-recaptcha" data-sitekey="6LejNDoUAAAAAMRvQvDBNE5BDkQvjkXSplnCAJZK"></div>
                                </div>
                            </div>
                              <button type="submit" class="button-two btn_sub btn">Submit</button>
                           </form>

                        </div>
                     </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
        <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDOEiKBWEe6ChWV0YsnLWWncVMp0hkQ84U'></script>
        <div style='overflow:hidden;height:400px;width:100%;margin-bottom:-20px;'>
            <div id='gmap_canvas' style='height:400px;width:100%;'></div>
            <style>
                #gmap_canvas img {
                    max-width: none!important;
                    background: none!important
                }
            </style>
        </div> <a href='http://maps-website.com/'  style="visibility:hidden;">maps-website</a>
        <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=bf831a3b983efdb4bb6ca94c8cc382a1e6f98ffa'></script>
        <script type='text/javascript'>
            function init_map() {
                var myOptions = {
                    zoom: 15,
                    center: new google.maps.LatLng(17.4518157, 78.39340349999998),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
                marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(17.4518157, 78.39340349999998)
                });
                infowindow = new google.maps.InfoWindow({
                    content: '<strong>Mbr Informatics</strong><br>#658, madhapur 100 feet road<br>500082 Hyderabad<br>'
                });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker);
                });
                infowindow.open(map, marker);
            }
            google.maps.event.addDomListener(window, 'load', init_map);
        </script>
    </section>
               <?php include 'layouts/footer.php'; ?>

        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a>
        </div>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
<!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script>
        
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//### Variables
var player;
var playerModal = $('#playerModal');

//### Youtube API
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: 'L2Ew6JzfZC8'
    });
}

//### Modal Controls (http://getbootstrap.com/javascript/#modals)
// Modal when show, begin to play video
playerModal.on('show.bs.modal', function (e) {
    player.playVideo();
});

// Modal when hidden, pause or stop playing video
playerModal.on('hidden.bs.modal', function (e) {
    player.pauseVideo();
    //player.stopVideo();
});
    </script>