<?php include 'layouts/header.php'; ?>
<section class="home-section home-full-height slider-cls" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(assets/images/slider3.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">We Build Awesome <br>Websites</div>
                                <div class="container">
                                    <div  class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <a class="mb-40 transition colelem btn_sub sub_btn" href="<?php echo base_url();?>About">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="restaurant-page-header" style="background-image:url(assets/images/slider1.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content">
                                <div class="font-alt mb-30 titan-title-size-1">We offer the best solutions</div>
                                <div class="font-alt mb-40 titan-title-size-4">We are a team of Creative <br>Web Designers & Web Developers</div>
                                <div class="container">
                                    <div  class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <a class="mb-40 transition colelem btn_sub sub_btn" href="<?php echo base_url();?>About">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section  class="gradient">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <a href="http://www.apdascac.ap.gov.in/" target="_blank"><img src="assets/images/apdascas.png" class="img_c1"></a>
                        </div>
                        <div class="col-md-3">
                            <a href="http://dooogle.in/"><img src="assets/images/doogle.png" target="_blank" class="img_c1"></a>
                        </div>
                        <div class="col-md-3">
                            <a href="https://h3sglobal.us/"><img src="assets/images/h3s.png" target="_blank" class="img_c1"></a>
                        </div>
                        <div class="col-md-3">
                            <a href="http://www.conduitmuzic.com/"><img src="assets/images/conduit.png" target="_blank" class="img_c1"></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>OUR Services</h2>
                        <p>Our services covers range of website design services whether you are a small-size local business or a large-size corporate firm with a requirement of quality website design services.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="hea_12">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="cont_1">
                                    <div class="gradient grad_trans">
                                        <i class="fa fa-laptop" aria-hidden="true"></i>
                                    </div>
                                    <h3>Web Design</h3>
                                    <p>Taking a deeper dive into your requirements, we aim to help taking your business to the next level.</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="cont_1">
                                    <div class="gradient grad_trans">
                                        <i class="fa fa-cogs" aria-hidden="true"></i>
                                    </div>
                                    <h3>Web Development</h3>
                                    <p>Having worked with clients,the expertise we gained is passed on automatically to every new client.</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="cont_1">
                                    <div class="gradient grad_trans">
                                        <i class="fa fa-mobile" aria-hidden="true"></i>
                                    </div>
                                    <h3>Mobile Apps</h3>
                                    <p>We provide high end mobile app services to achieve the best quality to boost business through your mobile app</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="cont_1">
                                    <div class="gradient grad_trans">
                                        <i class="fa fa-comments-o" aria-hidden="true"></i>
                                    </div>
                                    <h3>Chatbot</h3>
                                    <p>Bots upgrade the guest service experience, acting as a personal travel assistant and virtual concierge.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="video">
            <div class="video_section">
                <div class="container-fluid">
                    <div class="row">
                        <h4>Discover our Best Features</h4>
                        <h2>INTRODUCING MBR</h2>
                        <div class="jumbotron text-center container">
                            <div class="col-md-8 col-md-offset-4">
                                <a href='#myModal' class='transition colelem btn_sub sub_btn vbtn' data-toggle='modal' data-target='#playerModal'>Video About MBR</a>
                            </div>
                        </div>
                        <div class="modal fade" id='playerModal' role='dialog' tabindex='-1'>
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button class="close" aria-label='Close' data-dismiss='modal', type='button'>
                                  <span aria-hidden='true'>×</span></button>
                                <h4 class="modal-title">Video title</h4>
                              </div>
                              <div class="modal-body">
                                <div class="embed-responsive embed-responsive-16by9">
                                  <div id="player" class="embed-responsive-item"></div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss='modal' type='button'>
                                    Close
                                </button>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>E-commerce Design & Development</h2>
                        <p>We works closely with the client team and capable of coping with uncertainty and equipped to achieve quicker solutions.</p>
                    </div>
                </div>
                <div class="screen1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="screen1_cont">
                                    <h2>E-Commerce</h2>
                                    <h5>Our ecommerce system is the result of years of experience and dedication in the field with a goal to come out with the best of services and facilities. </h5>
                                    <h5>Starting from basic website designs, including CMS and online store building to highly complex business website apps and design solutions, we will customize the best of web development solutions for you.Our services has a wide array of features...</h5>
                                    <div class="transition colelem btn_sub read1">
                                        <a class="btn_sub1" href="<?php echo base_url();?>Services/ecommerce"><span>Read more</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 img_srv">
                                <img src="assets/images/ecomm.jpg" class="img-responsive animate pull-right mart10">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd bg_1">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>Digital Marketing</h2>
                        <p>The balance of skills and knowledge on technology permits MBR to act as a trustworthy product development company.</p>
                    </div>
                </div>
                <div class="screen1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-7 img_srv">
                                <img src="assets/images/digital.jpg" class="img-responsive animate mart10">
                            </div>
                            <div class="col-md-5">
                                <div class="screen1_cont">
                                    <h2>Digital Marketing</h2>
                                    <h5>Developing a long-term and reliable marketing strategy that will flourish and live a longer life, while ensuring your customers stay with you and tell the world the tales of your business’s awesomeness transforming our custom-made business strategies into real time success.</h5>
                                    <p>Our digital marketing company formulates creative marketing strategies that help you reach out to the right people...</p>
                                    <div class="transition colelem btn_sub read1">
                                        <a class="btn_sub1" href="<?php echo base_url();?>Services/digitalmarketing"><span>Read more</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>OUR PORTFOLIO</h2>
                        <p>Our fictional Makeover to the existing Websites</p>
                    </div>
                </div>
                <div class="portfolio">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <a href="<?php echo base_url();?>Portfolio/apdascac" target="_blank"><img src="assets/images/portfolio/apdascac.png" alt="APDASCAC" class="img-responsive animate"></a>
                                    </div>
                                    <a href="<?php echo base_url();?>Portfolio/apdascac" target="_blank"><h4>A.P.D.A.S.C.A.C</h4></a>
                                    <a href="<?php echo base_url();?>Portfolio/apdascac" target="_blank"><p><i class="fa fa-tag"></i>Government</p></a>
                                </div>
                                <div class="div20"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <a href="<?php echo base_url();?>Portfolio/conduitmuzic" target="_blank"><img src="assets/images/portfolio/cm.png" alt="Conduit Muzic"  class="img-responsive animate"></a>
                                    </div>
                                    <a href="<?php echo base_url();?>Portfolio/conduitmuzic" target="_blank"><h4>Conduit Muzic</h4></a>
                                    <a href="<?php echo base_url();?>Portfolio/conduitmuzic" target="_blank"><p><i class="fa fa-tag"></i>Music</p></a>
                                </div>
                                <div class="div20"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <a href="<?php echo base_url();?>Portfolio/sarithareddy" target="_blank"><img src="assets/images/portfolio/saritha.png" alt="Saritha Reddy"  class="img-responsive animate"></a>
                                    </div>
                                    <a href="<?php echo base_url();?>Portfolio/sarithareddy" target="_blank"><h4>Saritha Reddy</h4></a>
                                    <a href="<?php echo base_url();?>Portfolio/sarithareddy" target="_blank"><p><i class="fa fa-tag"></i>E-Commerce</p></a>
                                </div>
                                <div class="div20"></div>
                            </div>
                        </div>
                        <div class="div20"></div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <a href="<?php echo base_url();?>Portfolio/h3sglobal" target="_blank"><img src="assets/images/portfolio/h3s.png" alt="H3S Global"  class="img-responsive animate"></a>
                                    </div>
                                    <a href="<?php echo base_url();?>Portfolio/h3sglobal" target="_blank"><h4>H3S Global</h4></a>
                                    <a href="<?php echo base_url();?>Portfolio/h3sglobal" target="_blank"><p><i class="fa fa-tag"></i>Software</p></a>
                                </div>
                                <div class="div20"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <a href="<?php echo base_url();?>Portfolio/coresofttech" target="_blank"><img src="assets/images/portfolio/core.png" alt="Coresoft Technologies"  class="img-responsive animate"></a>
                                    </div>
                                    <a href="<?php echo base_url();?>Portfolio/coresofttech" target="_blank"><h4>Core Soft Technologies</h4></a>
                                    <a href="<?php echo base_url();?>Portfolio/coresofttech" target="_blank"><p><i class="fa fa-tag"></i>Software</p></a>
                                </div>
                               <div class="div20"></div> 
                            </div>
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <a href="<?php echo base_url();?>Portfolio/budgetcars" target="_blank"><img src="assets/images/portfolio/budget.png" alt="Budget" class="img-responsive animate"></a>
                                    </div>
                                    <a href="<?php echo base_url();?>Portfolio/budgetcars" target="_blank"><h4>Budget Cars</h4></a>
                                    <a href="<?php echo base_url();?>Portfolio/budgetcars" target="_blank"><p><i class="fa fa-tag"></i>Car Traders</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn_sec">
                        <div class="transition colelem btn_sub btn-sub1">
                            <span><a href="<?php echo base_url();?>Portfolio" class="btn_sub1" target="_blank">View more</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="numbers">
            <div class="numbers_section">
                <div class="container">
                    <div class="numbers_cont">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="numbers_1">
                                        <h1>+18</h1>
                                        <h4>Awards Win</h4>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="numbers_1">
                                        <h1>+6</h1>
                                        <h4>Years of Experience</h4>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="numbers_1">
                                        <h1>+54</h1>
                                        <h4>Finished Projects</h4>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="numbers_1">
                                        <h1>24/7</h1>
                                        <h4>Support</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="fea_bg1">
            <div class="div20"></div>
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h5>We Will Modify the Old Websites</h5>
                        <h2>Website Re-Design</h2>
                        <p>Here You can Notice the Difference between the old Websites and the new Websites that has been Re-Designed by us</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pm0">
                              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                  <div class="item sds active">
                                    <img src="assets/images/af/1.png" alt="Los Angeles">
                                  </div>
                                  <div class="item sds">
                                    <img src="assets/images/af/2.png" alt="Chicago">
                                  </div>
                                    <div class="item sds">
                                    <img src="assets/images/af/3.png" alt="Chicago">
                                  </div>
                                  <div class="item sds">
                                    <img src="assets/images/af/4.png" alt="New York">
                                  </div>
                                    <div class="item sds">
                                    <img src="assets/images/af/5.png" alt="New York">
                                  </div>
                                </div>
                                  <h3 class="bs">Before</h3>
                              </div>
                        </div>
                        <div class="col-md-6 pm0">
                              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                  <div class="item sds active">
                                    <img src="assets/images/af/1.png" alt="Los Angeles">
                                  </div>
                                  <div class="item sds">
                                    <img src="assets/images/af/2.png" alt="Chicago">
                                  </div>
                                    <div class="item sds">
                                    <img src="assets/images/af/3.png" alt="Chicago">
                                  </div>
                                  <div class="item sds">
                                    <img src="assets/images/af/4.png" alt="New York">
                                  </div>
                                    <div class="item sds">
                                    <img src="assets/images/af/5.png" alt="New York">
                                  </div>
                                </div>
                              </div>
                            <h3 class="bs">After</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="div20"></div>
        </section>
        <section class="bgt1">
            <div class="container-fluid">
                <div class="row">
                    <div id="testimonial4" class="testimonials carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="no" data-interval="3000" data-duration="1000">
                        <div class="testimonial4_header">
                            <h6>What Our Clients Say</h6>
                            <h4>what our clients are saying</h4>
                        </div>
                        <ol class="carousel-indicators">
                            <li data-target="#testimonial4" data-slide-to="0" class="active"></li>
                            <li data-target="#testimonial4" data-slide-to="1"></li>
                            <li data-target="#testimonial4" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <div class="testimonial4_slide">
                                    <img src="assets/images/ap.png" class="img-circle img-responsive" />
                                    <p>MBR has been working on our web design projects from the past 2 years. Their work is commendable and we're satisfied with their support.</p>
                                    <h4>Andhra Pradesh Government</h4>
                                    <span class="inf_a">Andhra Pradesh Differently Abled & Senior Citizens Assistance Corporation</span>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial4_slide">
                                    <img src="assets/images/sr.png" class="img-circle img-responsive" />
                                    <p>MBR team was designed our E-Commerce company website, a top class service in affordable price. Our relationship is cordial and fruitful.</p>
                                    <h4>Saritha Reddy</h4>
                                    <span class="inf_a">Director, New Jersey, USA</span>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial4_slide">
                                    <img src="assets/images/clients/vijay.png" class="img-circle img-responsive" />
                                    <p>We contacted MBR Informatics to design our company website. We're completely 100% happy with their web design and SEO services.</p>
                                    <h4>Vijay Madala</h4>
                                    <span class="inf_a">CEO, New Jersey, USA</span>
                                </div>
                            </div>
                        </div>
                        <a class="left carousel-control" href="#testimonial4" role="button" data-slide="prev">
                            <span class="fa fa-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#testimonial4" role="button" data-slide="next">
                            <span class="fa fa-chevron-right"></span>
                        </a>
                    </div>
                    </div>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>CONTACT US</h2>
                        <p>PLEASE FILL YOUR DETAILS WE WILL CALL YOU BACK</p>
                    </div>
                </div>
                    <div class="contact_form">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-8 col-md-offset-2">
                        <div class="well text-center">
                            <form action="<?php echo base_url()?>Contact/sendmail" method="post">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input class="form-control" id="id_fname" maxlength="100" name="f_name" placeholder="First name *" type="text" required>
                                    </div>
                                </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                        <input class="form-control" id="id_lname" maxlength="100" name="l_name" placeholder="Last name *" type="text" required>
                                    </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                     <input class="form-control" id="id_email" name="email" placeholder="E-mail *" type="email" required>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                     <input class="form-control" id="id_assunto" maxlength="100" name="Phone" placeholder="Phone Number *" type="text" required>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                     <input class="form-control" id="id_assunto" maxlength="100" name="companyname" placeholder="Company Name" type="text" / required>
                                  </div>
                              </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <select class="form-control" id="Service" required>
                                      <option>Select Service</option>
                                      <option>Web Designing</option>
                                      <option>Web Development</option>
                                      <option>E-Commerce Development</option>
                                      <option>Digital Marketing</option>
                                      <option>Consulting Services</option>
                                      <option>Business Services</option>
                                      <option>Technology Services</option>
                                      <option>Outsourcing Services</option>
                                    </select>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="form-group">
                                     <textarea class="form-control" cols="40" id="id_mensagem" name="message" placeholder="Message *" rows="7" data-gramm="true" data-txt_gramm_id="ada73dea-e8d6-f606-09ad-cd5c85e12eb9" data-gramm_id="ada73dea-e8d6-f606-09ad-cd5c85e12eb9" spellcheck="false" data-gramm_editor="true" style="z-index: auto; position: relative; line-height: 22.8571px; font-size: 16px; transition: none; " required></textarea><grammarly-btn><div class="_1BN1N Kzi1t MoE_1 _2DJZN" style="z-index: 2; transform: translate(664px, 145px);"><div class="_1HjH7"><div title="Protected by Grammarly" class="_3qe6h">&nbsp;</div></div></div></grammarly-btn>
                                  </div>
                              </div>
                              <button type="submit" class="button-two btn_sub btn submit1">Submit</button>
                           </form>

                        </div>
                     </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'layouts/footer.php'; ?>
        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
<!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
        
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//### Variables
var player;
var playerModal = $('#playerModal');

//### Youtube API
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: 'L2Ew6JzfZC8'
    });
}

//### Modal Controls (http://getbootstrap.com/javascript/#modals)
// Modal when show, begin to play video
playerModal.on('show.bs.modal', function (e) {
    player.playVideo();
});

// Modal when hidden, pause or stop playing video
playerModal.on('hidden.bs.modal', function (e) {
    player.pauseVideo();
    //player.stopVideo();
});
    </script>
