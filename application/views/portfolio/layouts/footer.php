<div class="main">
            <div class="module-small bg-dark">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="widget">
                                <h5 class="widget-title font-alt">About MBR Informatics</h5>
                                <img src="<?php echo base_url();?>assets/images/logo.png" class="img-responsive foo_logo">
                                <p>we built our company on integrity, competence and innovation by delivering outstanding results to a broad range of clients across the world.</p>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="widget">
                                <h5 class="widget-title font-alt">Our Services</h5>
                                 <ul class="icon-list">
                                    <li><a href="<?php echo base_url();?>Services/webdesigning">Web Designing</a></li>
                                    <li><a href="<?php echo base_url();?>Services/webdevelopment">Web Development</a></li>
                                    <li><a href="<?php echo base_url();?>Services/applicationdevelopment">Application Development</a></li>
                                    <li><a href="<?php echo base_url();?>Services/digitalprocessautomation">Digital Process Automation</a></li>
                                    <li><a href="<?php echo base_url();?>Services/digitalmarketing">Digital Marketing</a></li>
                                    <li><a href="<?php echo base_url();?>Services/iot">IOT</a></li>
                                    <li><a href="<?php echo base_url();?>Services/salesforce">Salesforce</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="widget">
                                <h5 class="widget-title font-alt">Recent Projects</h5>
                                <ul class="icon-list f1">
                                    <li>
                                        <div class="cont_f1">
                                            <div class="col-md-4 pm0">
                                                <img src="<?php echo base_url();?>assets/images/portfolio/budget.png" class="f_img">
                                            </div>
                                            <div class="col-md-8 f_t1">
                                                <h4>Budget Cars</h4>
                                                <p>oct 12, 2017</p>
                                                <a href="http://13.59.102.214/cars/">www.budgetcars.com</a>
                                            </div>
                                        </div>
                                    </li>
                                    <div class="clearfix"></div>
                                    <li>
                                        <div class="cont_f1">
                                            <div class="col-md-4 pm0">
                                                <img src="<?php echo base_url();?>assets/images/portfolio/h3s.png" class="f_img">
                                            </div>
                                            <div class="col-md-8 f_t1">
                                                <h4>H3S Global</h4>
                                                <p>jan 12, 2017</p>
                                                <a href="http://www.h3sglobal.us/">www.h3sglobal.us</a>
                                            </div>
                                        </div>
                                    </li>
                                    <div class="clearfix"></div>
                                    <li>
                                        <div class="cont_f1">
                                            <div class="col-md-4 pm0">
                                                <img src="<?php echo base_url();?>assets/images/portfolio/cm.png" class="f_img">
                                            </div>
                                            <div class="col-md-8 f_t1">
                                                <h4>Conduit Muzic</h4>
                                                <p>jul 22, 2017</p>
                                                <a href="http://www.conduitmuzic.com/">www.conduitmuzic.com</a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="widget">
                                <h5 class="widget-title font-alt">Contact Us</h5>
                                <ul class="widget-posts">
                                    <li class="clearfix">
                                        <p class="foo_cont"><i class="fa fa-home"></i><span>Address:</span> #658, 3rd Floor, Ayyappa Socitey, Madhapur, Hyderabad, 500081, Telangana</p>
                                        <p class="foo_cont"><i class="fa fa-phone"></i><span>Phone:</span> 040 64562322</p>
                                        <p class="foo_cont"><i class="fa fa-mobile" aria-hidden="true"></i><span>Mobile:</span> +91 8686768612, </p>
                                        <p class="foo_cont">+91 8019255777</p>
                                        <p class="foo_cont"><i class="fa fa-envelope"></i><span>Email:</span> <a href="mailto:contact@mbrinformatics.com">contact@mbrinformatics.com</a></p>
                                    </li>
                                    <li class="clearfix">
                                        
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="divider-d">
            <footer class="footer bg-dark bg_f">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="copyright font-alt">&copy; 2017&nbsp;<a href="<?php echo base_url();?>"> MBR Informatics</a>, All Rights Reserved</p>
                        </div>
                        <div class="col-sm-6">
                            <div class="footer-social-links">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                <a href="#"><i class="fa fa-skype"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>