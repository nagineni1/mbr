        <?php include 'layouts/header.php'; ?>

        <section class="home-section home-full-height home-section_inner" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(<?php echo base_url()?>assets/images/about-bg.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content_inner">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">We Build Awesome
                                    <br>Websites</div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <!--<a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="section_padd" id="portfolio-container">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>JB RELIABLE PACKERS</h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 pm0">
                        <img src="<?php echo base_url();?>assets/images/portfolio/jpackers.png" class="img-responsive plimg">
                    </div>
                    <div class="port_details">
                        <div class="col-md-12 port_cont_inner">
                            <div class="col-md-6">
                                <div class="port_cont_inner_i">
                                    <i class="fa fa-link"></i>
                                    <h5>URL :</h5>
                                </div>
                                <a href="http://www.jbreliablepackers.com/" class="port_a">www.jbreliablepackers.com</a>
                            </div>
                            <div class="col-md-6">
                                <div class="port_cont_inner_i">
                                    <i class="fa fa-calendar"></i>
                                    <h5>Designed on :</h5>
                                </div>
                                <a href="#" class="port_a">november 2016</a>
                            </div>
                            <!--<div class="col-md-3">
                                <div class="port_cont_inner_i">
                                    <i class="fa fa-user"></i>
                                    <h5>Related Website :</h5>
                                </div>
                                <a href="www.apdascac.com" class="port_a">www.apdascac.ap.gov.in</a>
                            </div>
                            <div class="col-md-3">
                                <div class="port_cont_inner_i">
                                    <i class="fa fa-user"></i>
                                    <h5>Related Website :</h5>
                                </div>
                                <a href="www.apdascac.com" class="port_a">www.apdascac.ap.gov.in</a>
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="port_text">
                        <div class="col-md-12 pm0">
                            <div class="col-md-6">
                                <div class="port_ptext">
                                    <h3>APDASCAC</h3>
                                    <p>JB Reliable Packers with its headquarters in Hyderabad with services including packing & unpacking,warehousing services,international moving and so on. </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="port_ptext1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="portfolio">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <img src="<?php echo base_url();?>assets/images/portfolio/srestaa1.png" class="img-responsive animate">
                                    </div>
                                    <h4>Srestaa</h4>
                                    <p><i class="fa fa-tag"></i>Electrical</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <a href="<?php echo base_url();?>Portfolio/budgetcars" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/budget.png" class="img-responsive animate"></a>
                                    </div>
                                    <a href="<?php echo base_url();?>Portfolio/budgetcars" target="_blank"><h4>Budget Cars</h4></a>
                                    <a href="<?php echo base_url();?>Portfolio/budgetcars" target="_blank"><p><i class="fa fa-tag"></i>Car Traders</p></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <a href="<?php echo base_url();?>Portfolio/apdascac" target="_blank"><img src="<?php echo base_url();?>assets/images/portfolio/apdascac.png" class="img-responsive animate"></a>
                                    </div>
                                    <a href="<?php echo base_url();?>Portfolio/apdascac" target="_blank"><h4>APDASCAC</h4></a>
                                    <a href="<?php echo base_url();?>Portfolio/apdascac" target="_blank"><p><i class="fa fa-tag"></i>Government</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
                <?php include 'layouts/footer.php'; ?>

        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a>
        </div>
    </main>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
    <!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script>
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        //### Variables
        var player;
        var playerModal = $('#playerModal');

        //### Youtube API
        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                height: '390',
                width: '640',
                videoId: 'L2Ew6JzfZC8'
            });
        }

        //### Modal Controls (http://getbootstrap.com/javascript/#modals)
        // Modal when show, begin to play video
        playerModal.on('show.bs.modal', function(e) {
            player.playVideo();
        });

        // Modal when hidden, pause or stop playing video
        playerModal.on('hidden.bs.modal', function(e) {
            player.pauseVideo();
            //player.stopVideo();
        });
    </script>
    <script>
        //<!--    portfolio  JS  =================    -->

        $(function() {
            var selectedClass = "";
            $(".fil-cat").click(function() {
                selectedClass = $(this).attr("data-rel");
                $("#portfolio").fadeTo(100, 0.1);
                $("#portfolio div").not("." + selectedClass).fadeOut().removeClass('scale-anm');
                setTimeout(function() {
                    $("." + selectedClass).fadeIn().addClass('scale-anm');
                    $("#portfolio").fadeTo(300, 1);
                }, 300);

            });
        });
    </script>
</body>

</html>