        <?php include 'layouts/header.php'; ?>

        <section class="home-section home-full-height home-section_inner" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(<?php echo base_url()?>assets/images/services-bg.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content_inner">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">We offer the <br> BEST SERVICES</div>
                                <div class="container">
                                    <div  class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                           <!--<a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>OUR SERVICES</h2>
                        <p>We offer a wide array of services to cater to any of your web, mobile, or digital marketing requirements. Check out below:</p>
                    </div>
                </div>
                <div class="screen1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>Web Designing</h2>
                                    <h5>As a pivotal name in the field of website design Company in India, we firmly believe that web design is an art in itself that amalgamates creative mind, talent and best of the programming knowledge.</h5>
                                    <p>The designs, which were hot favourite, last year are old-fashioned items today.We boast of our design artists who have seen changes in the industry over the decades and are the masters of the trade. Their dedicated work have led us to the position of the best website design company in Hyderabad and we strive to maintain our...</p>
                                    <div class="transition colelem btn_sub">
                                        <span><a href="<?php echo base_url();?>Services/webdesigning" class="btn_aa">Read more</a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pm0">
                                <img src="<?php echo base_url();?>assets/images/design.png" class="img-responsive ser-img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd bg_1">
            <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6 pm0">
                                <img src="<?php echo base_url();?>assets/images/develop.jpg" class="img-responsive ser-img">
                            </div>
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>Web Development</h2>
                                    <h5>A website reflects brand image of a business venture irrespective of its size. A website holds the key for successful running of an online business and it is the most imperative means for portraying one's brand identity and enticing new prospective clients, clientele, and business associations.</h5>
                                    <p>Our web team is ever ready to build customized and personalized web designs suiting your web requirements.We encourage creative approaches and mindset in terms of design which appeals to one's preferred sense of style and technique.</p>
                                    <div class="transition colelem btn_sub">
                                        <span><a href="<?php echo base_url();?>Services/webdevelopment" class="btn_aa">Read more</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </section>
        <section class="section_padd">
             <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>E-Commerce</h2>
                                    <h5>Deliver a flawless shopping experience to your customers with our customized e-commerce solutions tailored to your needs.</h5>
                                    <p>Our ecommerce system is the result of years of experience and dedication in the field with a goal to come out with the best of services and facilities. Starting from basic website designs, including CMS and online store building to highly complex business website apps and design solutions, we will customize the best of web development solutions for you.Our services has a wide array of features...</p>
                                    <div class="transition colelem btn_sub">
                                        <span><a href="<?php echo base_url();?>Services/ecommerce" class="btn_aa">Read more</a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pm0">
                                <img src="<?php echo base_url();?>assets/images/ecommerce.png" class="img-responsive ser-img">
                            </div>
                        </div>
                    </div>
            </div>
        </section>
        <section class="section_padd bg_1">
            <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6 pm0">
                                <img src="<?php echo base_url();?>assets/images/digitalmarketing.png" class="img-responsive ser-img">
                            </div>
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>Digital Marketing</h2>
                                    <h5>Developing a long-term and reliable marketing strategy that will flourish and live a longer life, while ensuring your customers stay with you and tell the world the tales of your business’s awesomeness transforming our custom-made business strategies into real time success.</h5>
                                    <p>Our digital marketing company formulates creative marketing strategies that help you reach out to the right people with the right message at right time through a right channel. In SEO we go beyond ranking to give Return on investment to client.</p>
                                    <div class="transition colelem btn_sub">
                                        <span><a href="<?php echo base_url();?>Services/digitalmarketing" class="btn_aa">Read more</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </section>
        <section class="section_padd">
             <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>IT Consulting</h2>
                                    <h5>MBR Informatics offers a variety of high quality IT consulting services for solving project management, business analysis, system analysis, and other IT issues.Our skilled professional consultants deliver objective recommendations and tailored solutions to meet your business needs.</h5>
                                    <p>MBR Informatics has wide-ranging experience across various industries and utilizes a wide range of technologies. We also have a good understanding of industry trends along with hands-on experience in addressing specific management and technical problems.</p>
                                    <div class="transition colelem btn_sub">
                                        <span><a href="<?php echo base_url();?>Services/consultingservices" class="btn_aa">Read more</a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pm0">
                                <img src="<?php echo base_url();?>assets/images/it.png" class="img-responsive ser-img">
                            </div>
                        </div>
                    </div>
            </div>
        </section>
        <section class="section_padd bg_1">
            <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6 pm0">
                                <img src="<?php echo base_url();?>assets/images/BusinessApplications.jpg" class="img-responsive ser-img">
                            </div>
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>Business Services</h2>
                                    <h5>IT is redefining business in a significant way. IT’s impact transcends geographies, enabling enterprises gain a distinct competitive advantage. But, is investing in any kind of technology going to work for your business? </h5>
                                    <p>As businesses must provide appreciable returns to shareholders it is imperative that their IT-enabled investments provide measurable business outcomes.Our SAP specialists work with the objective of helping clients realize measurable business value from their investments.</p>
                                    <div class="transition colelem btn_sub">
                                        <span><a href="<?php echo base_url();?>Services/businessservice" class="btn_aa">Read more</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </section>
        <section class="section_padd">
             <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>Technology Services</h2>
                                    <h5>Besides enabling pervasive innovation, IT organizations need to focus on modernizing legacy systems while enhancing existing systems and applications. Automating enterprise processes plays a key role in enabling agility. To deliver on the expectations from business, IT departments must be able to utilize and manage large pools of historical organizational knowledge.</h5>
                                    <p>To stay ahead in today’s dynamic technology environment, IT departments have to simultaneously work on the old and the new.</p>
                                    <div class="transition colelem btn_sub">
                                        <span><a href="<?php echo base_url();?>Services/technologyservices" class="btn_aa">Read more</a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 pm0">
                                <img src="<?php echo base_url();?>assets/images/serv.jpg" class="img-responsive ser-img">
                            </div>
                        </div>
                    </div>
            </div>
        </section>
        <section class="section_padd bg_1">
            <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6 pm0">
                                <img src="<?php echo base_url();?>assets/images/outs.jpg" class="img-responsive ser-img">
                            </div>
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>OutSourcing Services</h2>
                                    <h5>At MBR Informatics, we offer Application Outsourcing Services that are geared towards revolutionizing the clients’ application outsourcing portfolio, while enabling them to garner higher and enhanced business value. </h5>
                                    <p>Increasingly, business and IT leaders are leveraging strategic sourcing solutions to reduce costs and access new technologies and best practices. The retained organization lies at the heart of moving from an in-sourced or staff-augmentation model to a Managed Services sourcing model.</p>
                                    <div class="transition colelem btn_sub">
                                        <span><a href="<?php echo base_url();?>Services/outsourcing" class="btn_aa">Read more</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </section>
        <?php include 'layouts/footer.php'; ?>
        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a>
        </div>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
<!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script>
        
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//### Variables
var player;
var playerModal = $('#playerModal');

//### Youtube API
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: 'L2Ew6JzfZC8'
    });
}

//### Modal Controls (http://getbootstrap.com/javascript/#modals)
// Modal when show, begin to play video
playerModal.on('show.bs.modal', function (e) {
    player.playVideo();
});

// Modal when hidden, pause or stop playing video
playerModal.on('hidden.bs.modal', function (e) {
    player.pauseVideo();
    //player.stopVideo();
});
    </script>
</body>

</html>
