        <?php include 'layouts/header.php'; ?>

        <section class="home-section home-full-height home-section_inner" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(<?php echo base_url()?>assets/images/about-bg.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content_inner">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">We Build Awesome <br>Websites</div>
                                <div class="container">
                                    <div  class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <!--<div class="row">
                    <div class="hea_1">
                        <h5>OUR BEST OFFERS</h5>
                        <h2>INTERFACE FEATURES</h2>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words.</p>
                    </div>
                </div>-->
                <div class="screen1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>Business Application Services</h2>
                                    <h5>In a constantly evolving business landscape, it is imperative to future-proof your business and realize the full potential of technology breakthroughs while sustaining profitability in a competitive world.</h5>
                                    <p>The enterprise technology landscape in several organizations lags behind business demands.</p>
                                    <p>This necessitates creation of a flexible and efficient technology landscape that responds to business demands in an agile and efficient manner.</p>
                                </div>
                            </div>
                            <div class="col-md-6 pm0">
                                <img src="<?php echo base_url();?>assets/images/businessapp.png" class="img-responsive ser-img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd bg_1">
            <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="screen1_cont">
                                <h5>Prepackaged business applications from leading global product vendors provide best-in-class business processes. </h5>
                                <p>This facilitates cutting-edge technologies to evolve a comprehensive IT landscape for your enterprise.</p>
                                <p>In addition, enterprises need technology expertise backed by deep industry knowledge to successfully execute business agendas with robust IT strategy initiatives.</p>
                                <p>To achieve competitive advantage, organizations are increasingly looking towards adoption of technology-driven business models to build their IT infrastructure and enterprise architecture.</p>
                            </div>
                        </div>
                    </div>
            </div>
        </section>
        <?php include 'layouts/footer.php'; ?>
        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a>
        </div>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
<!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script>
        
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//### Variables
var player;
var playerModal = $('#playerModal');

//### Youtube API
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: 'L2Ew6JzfZC8'
    });
}

//### Modal Controls (http://getbootstrap.com/javascript/#modals)
// Modal when show, begin to play video
playerModal.on('show.bs.modal', function (e) {
    player.playVideo();
});

// Modal when hidden, pause or stop playing video
playerModal.on('hidden.bs.modal', function (e) {
    player.pauseVideo();
    //player.stopVideo();
});
    </script>
</body>

</html>
