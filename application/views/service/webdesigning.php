        <?php include 'layouts/header.php'; ?>

        <section class="home-section home-full-height home-section_inner" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(<?php echo base_url()?>assets/images/about-bg.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content_inner">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">We design flexible and <br> responsive Websites</div>
                                <div class="container">
                                    <div  class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <!--<a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <!--<div class="row">
                    <div class="hea_1">
                        <h5>OUR BEST OFFERS</h5>
                        <h2>INTERFACE FEATURES</h2>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words.</p>
                    </div>
                </div>-->
                <div class="screen1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>Web Designing</h2>
                                    <p>As a pivotal name in the field of website design Company in India, we firmly believe that web design is an art in itself that amalgamates creative mind, talent and best of the programming knowledge.</p>
                                    <h5>The designs, which were hot favourite, last year are old-fashioned items today.We boast of our design artists who have seen changes in the industry over the decades and are the masters of the trade.</h5>
                                    <h5>Their dedicated work have led us to the position of the best website design company in Hyderabad and we strive to maintain our excellence without any compromise.</h5>
                                    <!--<div class="transition colelem btn_sub">
                                        <span>Read more</span>
                                    </div>-->
                                </div>
                            </div>
                            <div class="col-md-6 pm0">
                                <img src="<?php echo base_url();?>assets/images/design.png" class="img-responsive ser-img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd bg_1">
            <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="screen1_cont">
                                <h5>MBR Informatics is one of the best website designing company based in Hyderabad. We have been designing creative designs and templates for clients according to their requirement.</h5>
                                <p>Web designing has been evolved through the years. There is a lot of change that you can observe in the designs which are in use earlier and present. We have a team of professionals who are experts in designing various creative designs and templates. We have a successful track of record of completing major projects for various industries.</p>
                                <p>Our web designing services include :</p>
                                <ul class="fa_screen1 pm0">
                                    <li><i class="fa fa-check"></i>Responsive design</li>
                                    <li><i class="fa fa-check"></i>Logo designing</li>
                                    <li><i class="fa fa-check"></i>Brochure designing</li>
                                    <li><i class="fa fa-check"></i>Website redesigning</li>
                                    <li><i class="fa fa-check"></i>Content management in a website</li>
                                    <li><i class="fa fa-check"></i>Creative template designing</li>
                                    <li><i class="fa fa-check"></i>User friendly web designing</li>
                                    <li><i class="fa fa-check"></i>Compatable web designs for all screen resolutions</li>
                                </ul>
                                <p>Once we take inputs of client requirement, we make sure to give a PSD copy or a live demo for clients, so that he is clear with the sample web design and complete architecture of the website.</p>
                                <p>With the best web designers having latest web skills who can design a completely customized website. We design fresh, innovative and unique designs for each project which are efficient and cost effective. User experience is most important for any kind of website we design. A creative and attractive design will easily attract new customers for business.</p> 

                                <p>We are passionate and dedicated in designing the websites along with complete assistance for maintenance removing all kinds of bugs to make sure we provide a error free website with a best performance.</p>
                                <!--<div class="transition colelem btn_sub">
                                    <span>Read more</span>
                                </div>-->
                            </div>
                        </div>
                    </div>
            </div>
        </section>
        <section>
            <img src="<?php echo base_url();?>assets/images/services/design.PNG" class="img-responsive">
        </section>
        <?php include 'layouts/footer.php'; ?>
        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a>
        </div>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
<!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script>
        
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//### Variables
var player;
var playerModal = $('#playerModal');

//### Youtube API
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: 'L2Ew6JzfZC8'
    });
}

//### Modal Controls (http://getbootstrap.com/javascript/#modals)
// Modal when show, begin to play video
playerModal.on('show.bs.modal', function (e) {
    player.playVideo();
});

// Modal when hidden, pause or stop playing video
playerModal.on('hidden.bs.modal', function (e) {
    player.pauseVideo();
    //player.stopVideo();
});
    </script>
</body>

</html>
