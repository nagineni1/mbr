        <?php include 'layouts/header.php'; ?>

        <section class="home-section home-full-height home-section_inner" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(<?php echo base_url()?>assets/images/about-bg.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content_inner">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">Experience our <br>Consulting Services</div>
                                <div class="container">
                                    <div  class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                           <!-- <a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <!--<div class="row">
                    <div class="hea_1">
                        <h5>OUR BEST OFFERS</h5>
                        <h2>INTERFACE FEATURES</h2>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words.</p>
                    </div>
                </div>-->
                <div class="screen1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>IT Consulting</h2>
                                    <h5>MBR Informatics offers a variety of high quality IT consulting services for solving project management, business analysis, system analysis, and other IT issues.Our skilled professional consultants deliver objective recommendations and tailored solutions to meet your business needs.</h5>
                                    <p>MBR Informatics has wide-ranging experience across various industries and utilizes a wide range of technologies.</p>
                                    <p> We also have a good understanding of industry trends along with hands-on experience in addressing specific management and technical problems.</p>
                                </div>
                            </div>
                            <div class="col-md-6 pm0">
                                <img src="<?php echo base_url();?>assets/images/it.png" class="img-responsive ser-img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd bg_1">
            <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="screen1_cont">
                                <h5>We mainly intend to get you the best business value from technology, our main strategy is as follows,</h5>
                                <ul class="fa_screen1 pm0">
                                    <li><i class="fa fa-check"></i>To develop an IT strategy that matches the capability of technology, price and effectiveness to your business.</li>
                                    <li><i class="fa fa-check"></i>To ensure that all your applications and infrastructures are optimized to get the appropriate innovation, reliability and price.</li>
                                    <li><i class="fa fa-check"></i>To design and implement IT operating models that enables you to manage all your business needs.</li>
                                    <li><i class="fa fa-check"></i>To define and govern the technical architecture that gurantees reliable, secured, cost effective utilization of the technology, while having effectiveness to acquire evolving circumstances, and capable of managing technical innovation.</li>
                                </ul>
                                <h5>Information technology strategy :</h5>
                                        <p>We develop an IT strategy roadmaps to arrange IT plans along with the business strategy and build business cases that assist in measuring and governance of IT value.
                                        </p>
                                <h5>To optimize applications and infrastructures :</h5>
                                        <p>We make sure that all your applications and infrastructures are designed in such a way they are reliable and agile for your business needs. We optimize the utilization of IT modernization, and cloud technologies.</p>
                                <h5>Information Technology Operations :</h5>
                                        <p>To design and implement IT operating models that assist you to manage and govern IT service delivery quality, price.</p>
                                <h5>Technical Architecture :</h5>
                                        <p>Enables you to define, plan, measure and manage the technical architecture services that deliver the technology in an appropriate way.
                                        </p>
                                
                               <!-- <div class="transition colelem btn_sub">
                                    <span>Read more</span>
                                </div>-->
                            </div>
                        </div>
                    </div>
            </div>
        </section>
        <?php include 'layouts/footer.php'; ?>
        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a>
        </div>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
<!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script>
        
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//### Variables
var player;
var playerModal = $('#playerModal');

//### Youtube API
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: 'L2Ew6JzfZC8'
    });
}

//### Modal Controls (http://getbootstrap.com/javascript/#modals)
// Modal when show, begin to play video
playerModal.on('show.bs.modal', function (e) {
    player.playVideo();
});

// Modal when hidden, pause or stop playing video
playerModal.on('hidden.bs.modal', function (e) {
    player.pauseVideo();
    //player.stopVideo();
});
    </script>
</body>

</html>
