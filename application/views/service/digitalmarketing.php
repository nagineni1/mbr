        <?php include 'layouts/header.php'; ?>

        <section class="home-section home-full-height home-section_inner" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(<?php echo base_url()?>assets/images/about-bg.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content_inner">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">An efficient way to <br> grow your brand value</div>
                                <div class="container">
                                    <div  class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <!--<a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <!--<div class="row">
                    <div class="hea_1">
                        <h5>OUR BEST OFFERS</h5>
                        <h2>INTERFACE FEATURES</h2>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words.</p>
                    </div>
                </div>-->
                <div class="screen1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="screen1_cont">
                                    <h2>Digital Marketing</h2>
                                    <h5>Developing a long-term and reliable marketing strategy that will flourish and live a longer life, while ensuring your customers stay with you and tell the world the tales of your business’s awesomeness transforming our custom-made business strategies into real time success.</h5>
                                    <p>Our digital marketing company formulates creative marketing strategies that help you reach out to the right people with the right message at right time through a right channel.</p>
                                    <p> In SEO we go beyond ranking to give Return on investment to client.</p>
                                    <!--<div class="transition colelem btn_sub">
                                        <span>Read more</span>
                                    </div>-->
                                </div>
                            </div>
                            <div class="col-md-6 pm0">
                                <img src="<?php echo base_url();?>assets/images/digitalmarketing.png" class="img-responsive ser-img">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd bg_1">
            <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="screen1_cont">
                                <h5>Digital marketing has different strategies to promote the business in various ways using different channels through online. We provide digital marketing services to the client’s websites that are designed by us or the other websites. Basically some of the main strategies involve :</h5>
                                <ul class="fa_screen1 pm0">
                                    <li><i class="fa fa-check"></i>SEO:
                                        Search engine optimization is making your website search engine friendly so that whenever a user searches for a particular keyword, results are shown in the first page for the keywords we place in our website. Search engine optimization is mainly done as off page and on page. It has various factors such as making proper URL structure, content management, keyword positions etc.</li>
                                    <li><i class="fa fa-check"></i>SEM : Search engine marketing mainly involves doing the paid campaigns or PPC campaigns for a website. These campaigns can be made with the help of Google Adwords, you can run these campaigns for a particular time to promote the website. We have experts who have been creating the paid campaigns for the last five years which has got great results inturn.</li>
                                    <li><i class="fa fa-check"></i>SMO : Social media optimization involves promoting the business or services through social media channels like facebook, twitter, instagram, pinterest etc. This is completely free process where you can make promotions through joining the groups.</li>
                                    <li><i class="fa fa-check"></i>SMM : Social media marketing is mainly promoting your website or business or services through paid campaigns in social media. We can run these ads by adding demographics per each ad, we can also make different type of ads in social media like text, video, images etc. We have a digital marketing team who has more than 5 years experience in making ads in social media.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
            </div>
        </section>
        <?php include 'layouts/footer.php'; ?>
        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a>
        </div>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
<!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script>
        
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//### Variables
var player;
var playerModal = $('#playerModal');

//### Youtube API
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: 'L2Ew6JzfZC8'
    });
}

//### Modal Controls (http://getbootstrap.com/javascript/#modals)
// Modal when show, begin to play video
playerModal.on('show.bs.modal', function (e) {
    player.playVideo();
});

// Modal when hidden, pause or stop playing video
playerModal.on('hidden.bs.modal', function (e) {
    player.pauseVideo();
    //player.stopVideo();
});
    </script>
</body>

</html>
