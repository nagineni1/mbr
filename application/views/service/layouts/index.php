<?php include 'layouts/header.php'; ?>
<section class="home-section home-full-height" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(assets/images/slider3.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">We Build Awesome <br>Websites</div>
                                <div class="container">
                                    <div  class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="restaurant-page-header" style="background-image:url(assets/images/slider1.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content">
                                <div class="font-alt mb-30 titan-title-size-1">We offer the best solutions</div>
                                <div class="font-alt mb-40 titan-title-size-4">We are a team of Creative <br>Web Designers & Web Developers</div>
                                <div class="container">
                                    <div  class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section  class="gradient">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <img src="assets/images/apdascas.png" class="img_c1">
                        </div>
                        <div class="col-md-3">
                            <img src="assets/images/doogle.png" class="img_c1">
                        </div>
                        <div class="col-md-3">
                            <img src="assets/images/h3s.png" class="img_c1">
                        </div>
                        <div class="col-md-3">
                            <img src="assets/images/conduit.png" class="img_c1">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>INTERFACE FEATURES</h2>
                        <p>Our services covers range of website design services whether you are a small-size local business or a large-size corporate firm with a requirement of quality website design services.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="hea_12">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="cont_1">
                                    <div class="gradient grad_trans">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <h3>Web Design</h3>
                                    <p>Taking a deeper dive into your requirements, we aim to help taking your business to the next level.</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="cont_1">
                                    <div class="gradient grad_trans">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <h3>Web Development</h3>
                                    <p>Having worked with clients,the expertise we gained is passed on automatically to every new client.</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="cont_1">
                                    <div class="gradient grad_trans">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <h3>Mobile Apps</h3>
                                    <p>We provide high end mobile app services to achieve the best quality to boost business through your mobile app</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="cont_1">
                                    <div class="gradient grad_trans">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <h3>Chatbot</h3>
                                    <p>Bots upgrade the guest service experience, acting as a personal travel assistant and virtual concierge.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="video">
            <div class="video_section">
                <div class="container-fluid">
                    <div class="row">
                        <h4>Discover our Best Features</h4>
                        <h2>INTRODUCING MBR</h2>
                        <div class="jumbotron text-center container">
                            <div class="col-md-8 col-md-offset-4">
                                <a href='#myModal' class='transition colelem btn_sub sub_btn' data-toggle='modal' data-target='#playerModal'>Video About MBR</a>
                            </div>
                        </div>
                        <div class="modal fade" id='playerModal' role='dialog' tabindex='-1'>
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button class="close" aria-label='Close' data-dismiss='modal', type='button'>
                                  <span aria-hidden='true'>×</span></button>
                                <h4 class="modal-title">Video title</h4>
                              </div>
                              <div class="modal-body">
                                <div class="embed-responsive embed-responsive-16by9">
                                  <div id="player" class="embed-responsive-item"></div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss='modal' type='button'>
                                    Close
                                </button>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>OUR ADDITIONAL SERVICES</h2>
                        <p>We works closely with the client team and capable of coping with uncertainty and equipped to achieve quicker solutions.</p>
                    </div>
                </div>
                <div class="screen1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="screen1_cont">
                                    <h2>E-Commerce</h2>
                                    <p> In today’s era of technology, e-commerce solutions have become the very important choice for many companies as there are various companies who have a great interest in developing the online shop.</p>
                                    <h5>Advantages of E-commerce solution:</h5>
                                    <ul class="fa_screen1 pm0">
                                        <li><i class="fa fa-check"></i>Quick and Convenient</li>
                                        <li><i class="fa fa-check"></i>Acquire Product Description</li>
                                        <li><i class="fa fa-check"></i>Flexibility and efficiency</li>
                                    </ul>
                                    <div class="transition colelem btn_sub">
                                        <span>Read more</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 img_srv">
                                <img src="assets/images/ecomm.jpg" class="img-responsive animate">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd bg_1">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h2>OUR ADDITIONAL SERVICES</h2>
                        <p>The balance of skills and knowledge on technology permits MBR to act as a trustworthy product development company.</p>
                    </div>
                </div>
                <div class="screen1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-7 img_srv">
                                <img src="assets/images/digital.jpg" class="img-responsive animate">
                            </div>
                            <div class="col-md-5">
                                <div class="screen1_cont">
                                    <h2>Digital Marketing</h2>
                                    <p> We offer Marketing services to make your online business more profitable and to get more leads.</p>
                                    <h5>The following are the services we provide:</h5>
                                    <ul class="fa_screen1 pm0">
                                        <li><i class="fa fa-check"></i>Search Engine Optimization</li>
                                        <li><i class="fa fa-check"></i>Social Media Marketing</li>
                                        <li><i class="fa fa-check"></i>Search Engine Marketing</li>
                                    </ul>
                                    <div class="transition colelem btn_sub">
                                        <span>Read more</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h5>OUR PORTFOLIO</h5>
                        <p>Our fictional Makeover to the existing Websites</p>
                    </div>
                </div>
                <div class="portfolio">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <img src="assets/images/p1.jpg" class="img-responsive animate">
                                    </div>
                                    <h4>Your Work Title Here</h4>
                                    <p><i class="fa fa-tag"></i>Technology</p>
                                </div>
                                <div class="div20"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <img src="assets/images/p2.jpg" class="img-responsive animate">
                                    </div>
                                    <h4>Your Work Title Here</h4>
                                    <p><i class="fa fa-tag"></i>Technology</p>
                                </div>
                                <div class="div20"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <img src="assets/images/p3.jpg" class="img-responsive animate">
                                    </div>
                                    <h4>Your Work Title Here</h4>
                                    <p><i class="fa fa-tag"></i>Technology</p>
                                </div>
                                <div class="div20"></div>
                            </div>
                        </div>
                        <div class="div20"></div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <img src="assets/images/p4.jpg" class="img-responsive animate">
                                    </div>
                                    <h4>Your Work Title Here</h4>
                                    <p><i class="fa fa-tag"></i>Technology</p>
                                </div>
                                <div class="div20"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <img src="assets/images/p5.jpg" class="img-responsive animate">
                                    </div>
                                    <h4>Your Work Title Here</h4>
                                    <p><i class="fa fa-tag"></i>Technology</p>
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                <div class="port_cont">
                                    <div class="port_img">
                                        <img src="assets/images/p6.jpg" class="img-responsive animate">
                                    </div>
                                    <h4>Your Work Title Here</h4>
                                    <p><i class="fa fa-tag"></i>Technology</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn_sec">
                        <div class="transition colelem btn_sub">
                            <span>learn more</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="numbers">
            <div class="numbers_section">
                <div class="container">
                    <div class="numbers_cont">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="numbers_1">
                                        <h1>+18</h1>
                                        <h4>Awards Win</h4>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="numbers_1">
                                        <h1>+6</h1>
                                        <h4>Years of Experience</h4>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="numbers_1">
                                        <h1>+54</h1>
                                        <h4>Finished Projects</h4>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="numbers_1">
                                        <h1>24/7</h1>
                                        <h4>Support</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="fea_bg1">
            <div class="div20"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="hea_1">
                        <h5>OUR BEST OFFERS</h5>
                        <h2>INTERFACE FEATURES</h2>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pm0">
                              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                  <div class="item sds active">
                                    <img src="assets/images/af/1.png" alt="Los Angeles">
                                  </div>
                                  <div class="item sds">
                                    <img src="assets/images/af/2.png" alt="Chicago">
                                  </div>
                                    <div class="item sds">
                                    <img src="assets/images/af/3.png" alt="Chicago">
                                  </div>
                                  <div class="item sds">
                                    <img src="assets/images/af/4.png" alt="New York">
                                  </div>
                                    <div class="item sds">
                                    <img src="assets/images/af/5.png" alt="New York">
                                  </div>
                                </div>
                                  <h3 class="bs">Before</h3>
                              </div>
                        </div>
                        <div class="col-md-6 pm0">
                              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                  <div class="item sds active">
                                    <img src="assets/images/af/1.png" alt="Los Angeles">
                                  </div>
                                  <div class="item sds">
                                    <img src="assets/images/af/2.png" alt="Chicago">
                                  </div>
                                    <div class="item sds">
                                    <img src="assets/images/af/3.png" alt="Chicago">
                                  </div>
                                  <div class="item sds">
                                    <img src="assets/images/af/4.png" alt="New York">
                                  </div>
                                    <div class="item sds">
                                    <img src="assets/images/af/5.png" alt="New York">
                                  </div>
                                </div>
                              </div>
                            <h3 class="bs">After</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="div20"></div>
        </section>
        <section class="bgt1">
            <div class="container-fluid">
                <div class="row">
                    <div id="testimonial4" class="testimonials carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="no" data-interval="3000" data-duration="1000">
                        <div class="testimonial4_header">
                            <h6>What Our Clients Say</h6>
                            <h4>what our clients are saying</h4>
                        </div>
                        <ol class="carousel-indicators">
                            <li data-target="#testimonial4" data-slide-to="0" class="active"></li>
                            <li data-target="#testimonial4" data-slide-to="1"></li>
                            <li data-target="#testimonial4" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <div class="testimonial4_slide">
                                    <img src="assets/images/testimonials-img1.jpg" class="img-circle img-responsive" />
                                    <p>Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur.</p>
                                    <h4>Ben Hanna</h4>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial4_slide">
                                    <img src="assets/images/testimonials-img2.jpg" class="img-circle img-responsive" />
                                    <p>Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur.</p>
                                    <h4>Ben Hanna</h4>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial4_slide">
                                    <img src="assets/images/testimonials-img3.jpg" class="img-circle img-responsive" />
                                    <p>Lorem ipsum dolor sit amet adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur.</p>
                                    <h4>Ben Hanna</h4>
                                </div>
                            </div>
                        </div>
                        <a class="left carousel-control" href="#testimonial4" role="button" data-slide="prev">
                            <span class="fa fa-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#testimonial4" role="button" data-slide="next">
                            <span class="fa fa-chevron-right"></span>
                        </a>
                    </div>
                    </div>
            </div>
        </section>
        <section class="section_padd">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h5>OUR BEST OFFERS</h5>
                        <h2>INTERFACE FEATURES</h2>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words.</p>
                    </div>
                </div>
                    <div class="contact_form">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-8 col-md-offset-2">
                        <div class="well text-center">
                            <form action="sendmail.php" method="post">
                              <input type="hidden" name="csrfmiddlewaretoken" value="jirY00s8p1JmoEVdHSHBrH4rXryWR0jU">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input class="form-control" id="id_fname" maxlength="100" name="f_name" placeholder="First name *" type="text">
                                    </div>
                                </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                        <input class="form-control" id="id_lname" maxlength="100" name="l_name" placeholder="Last name *" type="text">
                                    </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                     <input class="form-control" id="id_email" name="email" placeholder="E-mail" type="email">
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                     <input class="form-control" id="id_assunto" maxlength="100" name="Phone" placeholder="Phone Number *" type="text">
                                  </div>
                              </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                     <input class="form-control" id="id_site" name="site" placeholder="Seu site" type="url" />
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                     <input class="form-control" id="id_assunto" maxlength="100" name="assunto" placeholder="Assunto *" type="text" />
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="form-group">
                                     <textarea class="form-control" cols="40" id="id_mensagem" name="message" placeholder="Message *" rows="7" data-gramm="true" data-txt_gramm_id="ada73dea-e8d6-f606-09ad-cd5c85e12eb9" data-gramm_id="ada73dea-e8d6-f606-09ad-cd5c85e12eb9" spellcheck="false" data-gramm_editor="true" style="z-index: auto; position: relative; line-height: 22.8571px; font-size: 16px; transition: none; "></textarea><grammarly-btn><div class="_1BN1N Kzi1t MoE_1 _2DJZN" style="z-index: 2; transform: translate(664px, 145px);"><div class="_1HjH7"><div title="Protected by Grammarly" class="_3qe6h">&nbsp;</div></div></div></grammarly-btn>
                                  </div>
                              </div>
                              <button type="submit" class="button-two btn_sub btn">Submit</button>
                           </form>

                        </div>
                     </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'layouts/footer.php'; ?>
        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a>
        </div>
    </main>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
<!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
        
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

//### Variables
var player;
var playerModal = $('#playerModal');

//### Youtube API
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: 'L2Ew6JzfZC8'
    });
}

//### Modal Controls (http://getbootstrap.com/javascript/#modals)
// Modal when show, begin to play video
playerModal.on('show.bs.modal', function (e) {
    player.playVideo();
});

// Modal when hidden, pause or stop playing video
playerModal.on('hidden.bs.modal', function (e) {
    player.pauseVideo();
    //player.stopVideo();
});
    </script>
</body>

</html>
