        <?php include 'layouts/header.php'; ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        <section class="home-section home-full-height home-section_inner" id="home">
            <div class="hero-slider">
                <ul class="slides">
                    <li class="restaurant-page-header" style="background-image:url(<?php echo base_url()?>assets/images/careers-bg.jpg);">
                        <div class="titan-caption">
                            <div class="caption-content_inner">
                                <div class="font-alt mb-30 titan-title-size-1"> Welcome to MBR Informatics</div>
                                <div class="font-alt mb-40 titan-title-size-4">We Build Awesome
                                    <br>Websites</div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-4">
                                            <a class="mb-40 transition colelem btn_sub sub_btn" href="#menu">View More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <section class="section_padd" id="portfolio-container">
            <div class="container">
                <div class="row">
                    <div class="hea_1">
                        <h5>OUR BEST OFFERS</h5>
                        <h2>INTERFACE FEATURES</h2>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words.</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 pm0">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                  <div class="item sds active">
                                    <img src="<?php echo base_url();?>assets/images/c1.jpg" alt="Los Angeles">
                                  </div>
                                  <div class="item sds">
                                    <img src="<?php echo base_url();?>assets/images/c2.jpg" alt="Chicago">
                                  </div>
                                    <div class="item sds">
                                    <img src="<?php echo base_url();?>assets/images/c3.jpg" alt="Chicago">
                                  </div>
                                </div>
                              </div>
                    </div>
                    <?php   
    
                    foreach($job as $values){ ?>
                    <div class="port_details">
                        <div class="col-md-12 port_cont_inner">
                            <div class="col-md-4">
                                <div class="port_cont_inner_i">
                                    <i class="fa fa-user"></i>
                                    <h5>Position Required:</h5>
                                </div>
                                <a href="#" class="port_a"><?php echo $values->jobtitle; ?></a>
                            </div>
                            <div class="col-md-4">
                                <div class="port_cont_inner_i">
                                    <i class="fa fa-map-marker"></i>
                                    <h5>Location:</h5>
                                </div>
                                <a href="#" class="port_a"><?php echo $values->location; ?></a>
                            </div>
                            <div class="col-md-4">
                                <div class="port_cont_inner_i">
                                    <i class="fa fa-briefcase"></i>
                                    <h5>Job Type:</h5>
                                </div>
                                <a href="#" class="port_a"><?php echo $values->jobtype; ?></a>
                            </div>
                            <!--<div class="col-md-3">
                                <div class="port_cont_inner_i">
                                    <i class="fa fa-user"></i>
                                    <h5>Related Website :</h5>
                                </div>
                                <a href="www.apdascac.com" class="port_a">www.apdascac.ap.gov.in</a>
                            </div>-->
                        </div>
                    </div>
                </div>
                
                <div class="row">
                     
                    <div class="port_text">
                        <div class="col-md-12 pm0">
                            <div class="port_ptext">
                                <h3><?php echo $values->jobtitle; ?></h3>
                                <p><b><?php echo $values->jobtype; ?></b></p>
                            </div>
                            <div class="port_ptext1">
                                <p><?php echo $values->jobdescription; ?></p>
                        
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4>Responsibilities</h4>
                            <ul class="fa_screen1 pm0">
                                <li><?php echo $values->responsibilities; ?></li>
                                
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <h4>Requirements</h4>
                            <ul class="fa_screen1 pm0">
                                <li><?php echo $values->requirements; ?></li>
                                
                            </ul>
                            <div class="transition colelem btn_sub">
                                  <a data-toggle="collapse" class="btn_sub1" href="#collapse1" id="dns"><span>Apply Now</span></a>
                              </div>
                            <script>
                                $(document).ready(function(){
                                    $("#dns").click(function(){
                                        $(".form-collapse").show();
                                    });
                                });
                                </script>
                            <!--<div class="transition colelem btn_sub">
                                <a  href="<?php echo base_url();?>Careers/apply">Apply Now</a>
                            </div>-->
                        </div>
                    </div>
                    <?php } ?>
                </div>
                 <?php if(isset($success)) echo '<div class="alert alert-success">'.$success.'</div>'; ?>
                <div class="row form-collapse" style="display:none">
                    <div class="col-md-12">
                        <div class="panel-group">
                           <div class="panel">
                                <div id="collapse1" class="panel-collapse collapse">
                                

                                   <form action="" method="post" enctype="multipart/form-data">
                                      <div class="col-md-12">
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="name" class='control-label'>Name</label>
                                                <input type="name" class="form-control" name="name" id="name" placeholder="Enter Your Name" required>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="email" class='control-label'>E-mail</label>
                                                <input type="email" class="form-control" name="email" id="email" placeholder="Enter Your E-mail" required>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="number" class='control-label'>Phone Number</label>
                                                <input type="number" class="form-control" name="number" id="number" placeholder="Enter Your Phone Number" required>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="position" class='control-label'>Position Applied</label>
                                                <input type="name" class="form-control" name="position" id="position" placeholder="Enter Position Applied" required>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="jobtype" class='control-label'>Job Type</label>
                                                <select class="form-control" name="jobtype" id="jobtype" required>
                                                  <option>Select Your Option</option>
                                                  <option>Full Time</option>
                                                  <option>Part Time</option>
                                                  <option>Internship</option>
                                                  <option>Contract</option>
                                                </select>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="experience" class='control-label'>Experience</label>
                                                <select class="form-control" name="experience" id="experience" required>
                                                  <option>Select Your Option</option>
                                                  <option>Fresher</option>
                                                  <option>Internship</option>
                                                  <option>6 Months</option>
                                                  <option>1 Year</option>
                                                  <option>2 Years</option>
                                                  <option>3 Years</option>
                                                  <option>4 Years</option>
                                                  <option>5 Years</option>
                                                  <option>6 Years</option>
                                                  <option>> 6Years</option>
                                                </select>
                                              </div>
                                          </div>
                                      </div>
                                       <div class="col-md-12">
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="cctc" class='control-label'>Current CTC</label>
                                                <input type="number" class="form-control" name="cctc" id="cctc" placeholder="Enter Your current ctc" required>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="expected" class='control-label'>Expected CTC</label>
                                                <input type="number" class="form-control" name="expected" id="expected" placeholder="Enter Your expected ctc" required>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group required">
                                                <label for="previous" class='control-label'>Previous Company Name</label>
                                                <input type="text" class="form-control" name="previous" id="previous" placeholder="company Name" required>
                                              </div>
                                        </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="image">Upload Resume</label>
                                                <input type="file" class="form-control-file" name="image" id="image" aria-describedby="fileHelp" required>
                                                <!--<small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>-->
                                              </div> 
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="col-md-12">
                                              <div class="form-group required">
                                                <label for="jobdescription" class='control-label'>Job Description</label>
                                                <textarea class="form-control" name="jobdescription" id="jobdescription" rows="7" required></textarea>
                                              </div>
                                          </div>
                                            <div class="transition colelem ss">
                                                <button class="submit btn btn_sub1">Apply Now</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php include 'layouts/footer.php'; ?>

        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a>
        </div>
    <script src="<?php echo base_url();?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/lib/bootstrap/dist/<?php echo base_url()?>bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?php echo base_url();?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?php echo base_url();?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?php echo base_url();?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/lib/smoothscroll.js"></script>
    <script src="<?php echo base_url();?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?php echo base_url();?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
    <!--<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDK2Axt8xiFYMBMDwwG1XzBQvEbYpzCvFU"></script>-->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script>
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        //### Variables
        var player;
        var playerModal = $('#playerModal');

        //### Youtube API
        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                height: '390',
                width: '640',
                videoId: 'L2Ew6JzfZC8'
            });
        }

        //### Modal Controls (http://getbootstrap.com/javascript/#modals)
        // Modal when show, begin to play video
        playerModal.on('show.bs.modal', function(e) {
            player.playVideo();
        });

        // Modal when hidden, pause or stop playing video
        playerModal.on('hidden.bs.modal', function(e) {
            player.pauseVideo();
            //player.stopVideo();
        });
    </script>
    <script>
        //<!--    portfolio  JS  =================    -->

        $(function() {
            var selectedClass = "";
            $(".fil-cat").click(function() {
                selectedClass = $(this).attr("data-rel");
                $("#portfolio").fadeTo(100, 0.1);
                $("#portfolio div").not("." + selectedClass).fadeOut().removeClass('scale-anm');
                setTimeout(function() {
                    $("." + selectedClass).fadeIn().addClass('scale-anm');
                    $("#portfolio").fadeTo(300, 1);
                }, 300);

            });
        });
    </script>