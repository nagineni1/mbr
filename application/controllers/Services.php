<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('services.php');
	}
    public function webdesigning()
	{
		$this->load->view('service/webdesigning');
	}
    public function responsivedesign()
	{
		$this->load->view('service/responsivedesign');
	}
     public function logodesign()
	{
		$this->load->view('service/logodesign');
	}
     public function broucherdesign()
	{
		$this->load->view('service/broucherdesign');
	}
     public function seo()
	{
		$this->load->view('service/seo');
	}
     public function sem()
	{
		$this->load->view('service/sem');
	}
     public function webdevelopment()
	{
		$this->load->view('service/webdevelopment');
	}
     public function ecommerce()
	{
		$this->load->view('service/ecommerce');
	}
     public function digitalmarketing()
	{
		$this->load->view('service/digitalmarketing');
	}
     public function consultingservices()
	{
		$this->load->view('service/consultingservices');
	}
     public function businessservices()
	{
		$this->load->view('service/businessservices');
	}
     public function technologyservices()
	{
		$this->load->view('service/technologyservices');
	}
     public function outsourcing()
	{
		$this->load->view('service/outsourcing');
	}
    public function strategy()
	{
		$this->load->view('service/strategy');
	}
    public function business()
	{
		$this->load->view('service/business');
	}
    public function digital()
	{
		$this->load->view('service/digital');
	}
    public function insights()
	{
		$this->load->view('service/insights');
	}
    public function learning()
	{
		$this->load->view('service/learning');
	}
    public function dataanalytics()
	{
		$this->load->view('service/dataanalytics');
	}
    public function salesforce()
	{
		$this->load->view('service/salesforce');
	}
    public function businessapplications()
	{
		$this->load->view('service/businessapplications');
	}
    public function sap()
	{
		$this->load->view('service/sap');
	}
    public function oracle()
	{
		$this->load->view('service/oracle');
	}
    public function applicationdevelopment()
	{
		$this->load->view('service/applicationdevelopment');
	}
    public function iot()
	{
	
        $this->load->view('service/iot');
	}
    public function digitalprocessautomation()
	{
		$this->load->view('service/digitalprocessautomation');
	}
    public function testing()
	{
		$this->load->view('service/testing');
	}
    public function enterprisemobility()
	{
		$this->load->view('service/enterprisemobility');
	}
    public function applicationoutsourcing()
	{
		$this->load->view('service/applicationoutsourcing');
	}
    public function businessprocessoutsourcing()
	{
		$this->load->view('service/businessprocessoutsourcing');
	}
    public function finance()
	{
		$this->load->view('service/finance');
	}
    public function humanresource()
	{
		$this->load->view('service/humanresource');
	}
    public function procurement()
	{
		$this->load->view('service/procurement');
	}
   }
