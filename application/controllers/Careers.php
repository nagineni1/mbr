<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Careers extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mainmodel');
        
    }
    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        
        $data['job'] = $this->Mainmodel->get_all('postjob');
		$this->load->view('careers.php',$data);
	}
    public function career($id)
	{
         
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png|GIF|JPG|PNG|jpeg|JPEG';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('image'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        
                }
                else
                {
                        $_POST['image'] = 'uploads/'.$this->upload->data()['file_name'];
                }
            
                if($this->Mainmodel->insert('applyjob',$_POST)){
                    $data['success'] = "Applied Job Sucessfullly";
                } 
        }
       
        $data['job'] = $this->Mainmodel->get_all_where('postjob',"id = '$id'");

		$this->load->view('career',$data);
	}
    public function career_post()
	{           
        $data = array();
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png|GIF|JPG|PNG|jpeg|JPEG';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('companylogo'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        
                }
                else
                {
                        $_POST['companylogo'] = 'uploads/'.$this->upload->data()['file_name'];
                }
            
                if($this->Mainmodel->insert('postjob',$_POST)){
                    $data['success'] = "Job Inserted Sucessfullly";
                } 
        }
       

		$this->load->view('career_post',$data);
	}
}
