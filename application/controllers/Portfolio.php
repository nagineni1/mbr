<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('portfolio');
	}
    public function apdascac()
	{
		$this->load->view('portfolio/apdascac');
	}
    public function accusaga()
	{
		$this->load->view('portfolio/accusaga');
	}
    public function budgetcars()
	{
		$this->load->view('portfolio/budgetcars');
	}
    public function conduitmuzic()
	{
		$this->load->view('portfolio/conduitmuzic');
	}
    public function coresofttech()
	{
		$this->load->view('portfolio/coresofttech');
	}
    public function sumascorp()
	{
		$this->load->view('portfolio/sumascorp');
	}
    public function h3sglobal()
	{
		$this->load->view('portfolio/h3sglobal');
	}
    public function sarithareddy()
	{
		$this->load->view('portfolio/sarithareddy');
	}
    public function somaraspa()
	{
		$this->load->view('portfolio/somaraspa');
	}
    public function amerident()
	{
		$this->load->view('portfolio/amerident');
	}
    public function sresta()
	{
		$this->load->view('portfolio/sresta');
	}
    public function jbpackers()
	{
		$this->load->view('portfolio/jbpackers');
	}
    public function sahakar()
	{
		$this->load->view('portfolio/sahakar');
	}
    public function echelon()
	{
		$this->load->view('portfolio/echelon');
	}
    public function fluidpower()
	{
		$this->load->view('portfolio/fluidpower');
	}
    public function h3sglobal1()
	{
		$this->load->view('portfolio/h3sglobal1');
	}
    public function tulasi()
	{
		$this->load->view('portfolio/tulasi');
	}
    public function kakatiya()
	{
		$this->load->view('portfolio/kakatiya');
	}
}
