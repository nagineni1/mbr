-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2017 at 10:21 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mbrinformatics`
--

-- --------------------------------------------------------

--
-- Table structure for table `applyjob`
--

CREATE TABLE `applyjob` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `number` varchar(30) NOT NULL,
  `position` varchar(50) DEFAULT NULL,
  `jobtype` varchar(2250) DEFAULT NULL,
  `experience` varchar(50) DEFAULT NULL,
  `cctc` varchar(50) DEFAULT NULL,
  `expected` varchar(50) DEFAULT NULL,
  `previous` varchar(50) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `jobdescription` varchar(225) DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applyjob`
--

INSERT INTO `applyjob` (`id`, `name`, `email`, `number`, `position`, `jobtype`, `experience`, `cctc`, `expected`, `previous`, `image`, `jobdescription`, `reg_date`) VALUES
(1, 'dfgdg', 'gdsfg@gbh.ghj', '4554655', 'dgf', 'Part Time', '4 Years', '38', '49', 'gfdg', 'uploads/yhuf.PNG', 'dfgvedfg', '2017-11-29 10:03:49'),
(2, 'dfgdg', 'gdsfg@gbh.ghj', '4554655', 'dgf', 'Part Time', '4 Years', '38', '49', 'gfdg', 'uploads/yhuf1.PNG', 'dfgvedfg', '2017-11-29 10:04:35');

-- --------------------------------------------------------

--
-- Table structure for table `postjob`
--

CREATE TABLE `postjob` (
  `id` int(6) UNSIGNED NOT NULL,
  `jobtitle` varchar(225) NOT NULL,
  `location` varchar(30) NOT NULL,
  `jobtype` varchar(50) DEFAULT NULL,
  `jobdescription` longtext,
  `companyname` varchar(50) DEFAULT NULL,
  `companylogo` varchar(50) DEFAULT NULL,
  `requirements` longtext,
  `responsibilities` longtext NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `postjob`
--

INSERT INTO `postjob` (`id`, `jobtitle`, `location`, `jobtype`, `jobdescription`, `companyname`, `companylogo`, `requirements`, `responsibilities`, `reg_date`) VALUES
(1, 'PHP Developer', 'Hyderabad', 'Full Time', 'We are looking for a PHP Developer who is dedicated to writes code that they are proud of and can hit the ground running.<br>A PHP Developer writes beautiful, fast PHP to a high standard, in a timely and scalable way that improves the code-base of our products in meaningful ways.<br>A PHP Developer writes beautiful, fast PHP to a high standard, in a timely and scalable way that improves the code-base of our products in meaningful ways.\r\n', 'MBR Informatics', 'Capture.PNG', '1.Proven software development experience in PHP.<br>\r\n2.Understanding of open source projects like Joomla, Drupal, Codeigniter framework etc.<br>\r\n3.Demonstrable knowledge of web technologies including HTML, CSS, Javascript, AJAX etc.<br>\r\n4.Good knowledge of relational databases, version control tools and of developing web services.<br>\r\n5.Passion for best design and coding practices and a desire to develop new bold ideas.<br>\r\n6.BS/MS degree in Computer Science, Engineering or a related subject.', '1.Write “clean”, well-designed code.<br>\r\n2.Produce detailed specifications.<br>\r\n3.Troubleshoot, test and maintain the core product software and databases to ensure strong optimization and functionality.<br>\r\n4.Contribute in all phases of the development lifecycle.<br>\r\n5.Follow industry best practices.<br>\r\n6.Develop and deploy new features to facilitate related procedures and tools if necessary.', '2017-11-30 09:14:24'),
(2, 'UI Designer', 'Hyderabad', 'Full Time', 'We are looking for a talented UI Designer to create amazing user experiences.  The ideal candidate should have an eye for clean and artful design, possess superior UI skills and be able to translate high-level requirements into interaction flows and artifacts, and transform them into beautiful, intuitive, and functional user interfaces.', 'MBR Informatics', 'Capture.PNG', '1.Proven UI experience.<br>\r\n2.Demonstrable UI design skills with a strong portfolio.<br>\r\n3.Solid experience in creating wireframes, storyboards, user flows, process flows and site maps.<br>\r\n4.Proficiency in Photoshop, Illustrator or other visual design and wire-framing tools.<br>\r\n5.Proficiency in HTML, CSS, and JavaScript for rapid prototyping.<br>\r\n6.Excellent visual design skills with sensitivity to user-system interaction.<br>\r\n7.Ability to present your designs and sell your solutions to various stakeholders.<br>\r\n8.Ability to solve problems creatively and effectively.<br>\r\n9.Up-to-date with the latest UI trends, techniques, and technologies.<br>\r\n10.Experience working in an Agile/Scrum development process.', '1.Collaborate with product management and engineering to define and implement innovative solutions for the product direction, visuals and experience.<br>\r\n2.Execute all visual design stages from concept to final hand-off to engineering.<br>\r\n3.Conceptualize original ideas that bring simplicity and user friendliness to complex design roadblocks.<br>\r\n4.Create wireframes, storyboards, user flows, process flows and site maps to effectively communicate interaction and design ideas.<br>\r\n5.Present and defend designs and key milestone deliverables to peers and executive level stakeholders.<br>\r\n6.Conduct user research and evaluate user feedback.<br>\r\n7.Establish and promote design guidelines, best practices and standards.', '2017-11-30 09:14:56'),
(3, 'BDE business development executives ( 2 Lead Generation, 1 Field Sales)', 'Hyderabad', 'Full time', 'We are looking for a talent marketing executive with Good communication skills who can handle the Client meetings effectively.', 'MBR Informatics', 'Capture.PNG', '1.Proficiency in MS Office and CRM software (e.g. Salesforce).<br>\r\n2.Proficiency in English.<br>\r\n3.Market knowledge.<br>\r\n4.Communication and negotiation skills.<br>\r\n5.Ability to build rapport.<br>\r\n6.Time management and planning skills.<br>\r\n7.BSc/BA in business administration, sales or relevant field.\r\n', '1.Pitch Ideas/marketing strategies to C Level executives.<br>\r\n2.Lead Generation for a digital marketing agency.<br>\r\n3.Take design brief and execute creative designs with the design team.<br>\r\n4.Suggest branding/marketing strategies.<br>\r\n5.Pitch in for New Orders and Customers.<br>\r\n6.Visiting new clients.<br>\r\n7.Execute branding exercises with the production team.<br>\r\n8.Responsible for networking & identifying the clients.<br>\r\n9.Required to be constantly updated with the domestic & international markets.<br>\r\n10.Gather market Intelligence on competitors and their activities.<br>\r\n11.Responsible for achieving targets set by the organization.', '2017-11-30 06:26:18'),
(4, 'Digital Marketing Executive', 'Hyderabad', 'Full time', 'We are looking for an SEO/SEM expert to manage all search engine optimization and marketing activities. You will be responsible for managing all SEO activities such as content strategy, link building and keyword strategy to increase rankings on all major search networks. You will also manage all SEM campaigns on Google, Yahoo and Bing in order to maximize ROI.', 'MBR Informatics', 'Capture.PNG', '1.Proficiency in MS Office and CRM software (e.g. Salesforce).<br>\r\n2.Proficiency in English.<br>\r\n3.Market knowledge.<br>\r\n4.Communication and negotiation skills.<br>\r\n5.Ability to build rapport.<br>\r\n6.Time management and planning skills.<br>\r\n7.BSc/BA in business administration, sales or relevant field.\r\n', '1.Execute tests, collect and analyze data and results, identify trends and insights in order to achieve maximum ROI in search campaigns.<br>\r\n2.Track, report, and analyze website analytics and PPC initiatives and campaigns.<br>\r\n3.Manage campaign expenses, staying on budget, estimating monthly costs and reconciling discrepancies.<br>\r\n4.Optimize copy and landing pages for search engine marketing.<br>\r\n5.Perform ongoing keyword discovery, expansion and optimization.<br>\r\n6.Research and implement search engine optimization recommendations.<br>\r\n7.Research and analyze competitor advertising links.<br>\r\n8.Develop and implement link building strategy.<br>\r\n9.Work with the development team to ensure SEO best practices are properly implemented on newly developed code.<br>\r\n10.Work with editorial and marketing teams to drive SEO in content creation and content programming.<br>\r\n11.Recommend changes to website architecture, content, linking and other factors to improve SEO positions for target keywords.\r\n', '2017-11-30 06:26:18'),
(5, 'Data Scientist', 'Hyderabad', 'Full time', 'We are looking for a Data Scientist to analyze large amounts of raw information to find patterns that will help improve our company. We will rely on you to build data products to extract valuable business insights.\r\nIn this role, you should be highly analytical with a knack for analysis, math and statistics. Critical thinking and problem-solving skills are essential for interpreting data. We also want to see a passion for machine-learning and research.', 'MBR Informatics', 'Capture.PNG', '1.Proficiency in MS Office and CRM software (e.g. Salesforce).<br>\r\n2.Proficiency in English.<br>\r\n3.Market knowledge.<br>\r\n4.Communication and negotiation skills.<br>\r\n5.Ability to build rapport.<br>\r\n6.Time management and planning skills.<br>\r\n7.BSc/BA in business administration, sales or relevant field.\r\n', '1.Identify valuable data sources and automate collection processes.<br>\r\n2.Undertake preprocessing of structured and unstructured data.<br>\r\n3.Analyze large amounts of information to discover trends and patterns.<br>\r\n4.Build predictive models and machine-learning algorithms.<br>\r\n5.Combine models through ensemble modeling.<br>\r\n6.Present information using data visualization techniques.<br>\r\n7.Propose solutions and strategies to business challenges.<br>\r\n8.Collaborate with engineering and product development teams.\r\n', '2017-11-30 08:07:33'),
(6, 'SMO:(social media optimization)', 'Hyderabad', 'Full time', 'Responsible to promote SaaS Product and Services through lead Generation on social media.<br>\r\nTo identify business opportunities and drive profitable new business with client mapping, focused social media campaigns, emails, etc.<br>\r\nManage entire lead generation life-cycle including prospecting, lead qualification, lead nurturing, and conversion.<br>\r\nTo cultivate, build and manage relationships with influencers and stakeholders through social channels.\r\n', 'MBR Informatics', 'Capture.PNG', '1.Business / Systems Analysis or Quality Assurance.<br>\r\n2.A degree in IT / Computer Science.<br>\r\n3.Knowledge in eliciting requirements and testing.<br>\r\n4.Experience in analyzing data to draw business-relevant conclusions and in data visualization techniques and tools.<br>\r\n5.Solid experience in writing SQL queries.<br>\r\n6.Basic knowledge in generating process documentation.<br>\r\n7.Strong written and verbal communication skills including technical writing skills.\r\n', '1.Must be an expert at social media publishing, content development and engagement.<br>\r\n2.Proven experience leading social campaigns that achieve company goals.<br>\r\n3.Demonstrated knowledge of social media storytelling and growth hacking techniques.<br>\r\n4.Create and manage lead acquisition campaigns, monitor performance, and track and report on campaign performance data.<br>\r\n5.Grow social media presence.<br>\r\n6.Participate in the planning process of email, webinar, event programs, and content creation.<br>\r\n7.Lead the execution of email programs.<br>\r\n8.Build and execute inbound marketing campaigns.<br>\r\n9.Develop an effective lead nurturing program for all inbound and outbound campaigns.<br>\r\n10.Meet aggressive lead generation numbers each quarter.\r\n', '2017-11-30 08:15:25'),
(7, 'Business Analyst', 'Hyderabad', 'Full time', 'We are looking for a Business Analyst who will be the vital link between our information technology capacity and our business objectives by supporting and ensuring the successful completion of analytical, building, testing and deployment tasks of our software product’s features.', 'MBR Informatics', 'Capture.PNG', '1.Business / Systems Analysis or Quality Assurance.<br>\r\n2.A degree in IT / Computer Science.<br>\r\n3.Knowledge in eliciting requirements and testing.<br>\r\n4.Experience in analyzing data to draw business-relevant conclusions and in data visualization techniques and tools.<br>\r\n5.Solid experience in writing SQL queries.<br>\r\n6.Basic knowledge in generating process documentation.<br>\r\n7.Strong written and verbal communication skills including technical writing skills.\r\n', '1.Define configuration specifications and business analysis requirements.<br>\r\n2.Perform quality assurance.<br>\r\n3.Define reporting and alerting requirements.<br>\r\n4.Own and develop relationship with partners, working with them to optimize and enhance our integration.<br>\r\n5.Help design, document and maintain system processes.<br>\r\n6.Report on common sources of technical issues or questions and make recommendations to product team.<br>\r\n7.Communicate key insights and findings to product team.<br>\r\n8.Constantly be on the lookout for ways to improve monitoring, discover issues and deliver better value to the customer.\r\n', '2017-11-30 08:15:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applyjob`
--
ALTER TABLE `applyjob`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `postjob`
--
ALTER TABLE `postjob`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applyjob`
--
ALTER TABLE `applyjob`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `postjob`
--
ALTER TABLE `postjob`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
